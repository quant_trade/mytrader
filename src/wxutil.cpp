#include "wxutil.h"

wxString utf2wxString(const char* str)
{
	static wxCSConv utf8cv(wxFONTENCODING_UTF8);
	try {
		return utf8cv.cMB2WX(str);
	}
	catch (...) {

	}
	return wxEmptyString;
}

std::string wxString2utf(const wxString& src)
{
	static wxCSConv cvutf(wxFONTENCODING_UTF8);
	return cvutf.cWX2MB(src.c_str());
}

wxString gbk2wxString(const char* str)
{
	static wxCSConv gbkcv(wxFONTENCODING_GB2312); 
	try {
		return gbkcv.cMB2WX(str);
	}
	catch (...) {

	}
	return wxEmptyString;
}

std::string wxString2gbk(const wxString& src)
{
	static wxCSConv cvGB2312(wxFONTENCODING_GB2312);
	return cvGB2312.cWX2MB(src.c_str());
}