#pragma once

#ifndef _H_ZQDB_IF_H_
#define _H_ZQDB_IF_H_

#include "zqdb.h"

#if defined(__cplusplus)
extern "C" {
#endif//

#pragma pack(push, 1)

struct ZQDB_INF
{
	int xmlflag;
	const char* xml;
	ZQDB_MSG_CB msg_cb;
	ZQDB_NOTIFY_CB notify_cb;
};

ZQDB_API_EXPORT char* ZQDBGetDeviceID(char* id, int len);

ZQDB_API_EXPORT bool ZQDBVerify(const char* info);

ZQDB_API_EXPORT size_t ZQDBFindData(const char* xml, int xmlflag);
ZQDB_API_EXPORT MDB_STATUS ZQDBClearData(const char* xml, int xmlflag);

ZQDB_API_EXPORT int ZQDBInit(ZQDB_INF* inf);
ZQDB_API_EXPORT void ZQDBTerm();

ZQDB_API_EXPORT const char* ZQDBXml();
ZQDB_API_EXPORT int ZQDBXmlFlag();

ZQDB_API_EXPORT int ZQDBStart();
typedef void(*ZQDBRun_CB)();
//wait=0表示不等待直接返回，非0表示阻塞等待，直到用户发送SIGINT(CTRL+C)/SIGTERM(kill)信号停止证券DB，
//如果设置了cb，wait值表示每wait秒数cb回调一次，如果不设置cb则证券DB每wait秒数自动检查系统运行状态
ZQDB_API_EXPORT void ZQDBRun(size_t wait, ZQDBRun_CB cb);

ZQDB_API_EXPORT void ZQDBCloseData(HZQDB* h, size_t count);

#define ZQDB_STOP_FLAG_NONE 0
#define ZQDB_STOP_FLAG_CLOSE 0X01

ZQDB_API_EXPORT void ZQDBStop(size_t flags);

ZQDB_API_EXPORT bool ZQDBStartRecord();
ZQDB_API_EXPORT void ZQDBStopRecord();

#pragma pack(pop)

#if defined(__cplusplus)
}

namespace zqdb {

}

#endif//

#endif//_H_ZQDB_IF_H_