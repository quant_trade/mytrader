#pragma once

#ifndef _H_ZQDB_CALC_MODULE_H_
#define _H_ZQDB_CALC_MODULE_H_

#include "calc.h"

#if defined(LIB_CALC_MODULE_API) && defined(WIN32)
#ifdef LIB_CALC_MODULE_API_EXPORT
#define CALC_MODULE_API_EXPORT __declspec(dllexport)
#else
#define CALC_MODULE_API_EXPORT __declspec(dllimport)
#endif
#else
#define CALC_MODULE_API_EXPORT 
#endif

#if defined(__cplusplus)
extern "C" {
#endif//

#pragma pack(push, 1)

//计算线程回调，每一个计算线程都会调用一次
typedef void(*ZQDBCalcModuleUpdate_CB)(HZQDB data, ZQDB_NOTIFY_TYPE type);

//ZQDB回调
typedef void(*ZQDBCalcModuleNotify_CB)(HZQDB data, ZQDB_NOTIFY_TYPE type);

typedef bool(*ZQDBCalcModuleExist_CB)(CALC_TYPE type, const char* name);
typedef int(*ZQDBCalcModuleNew_CB)(CALC_TYPE type, const char* name, void(*cb)(const char* src, void* data), void* data);
typedef int(*ZQDBCalcModuleLoad_CB)(CALC_TYPE type, const char* name, void(*cb)(const char* src, void* data), void* data);
typedef int(*ZQDBCalcModuleSave_CB)(CALC_TYPE type, const char* name, const char* src, size_t flags);
typedef int(*ZQDBCalcModuleDelete_CB)(CALC_TYPE type, const char* name);
typedef int(*ZQDBCalcModuleRename_CB)(CALC_TYPE type, const char* from, const char* to);

typedef int(*ZQDBCalcModuleCompile_CB)(CALC_TYPE type, const char* name, const char* src, void(*cb)(const char* err, void* data), void* data);
//typedef int(*ZQDBCalcModuleApply_CB)(CALC_TYPE type, const char* name, const char* src);

struct ZQDB_CALC_MODULE_INF
{
	int xmlflag;
	const char* xml;
	//tagCalcModuleInfo
	CALC_LANG_TYPE lang; //语言
	//
	ZQDBCalcModuleUpdate_CB update_cb;
	ZQDBCalcModuleNotify_CB notify_cb;
	ZQDBCalcModuleExist_CB exist_cb;
	ZQDBCalcModuleNew_CB new_cb;
	ZQDBCalcModuleDelete_CB delete_cb;
	ZQDBCalcModuleRename_CB rename_cb;
	ZQDBCalcModuleLoad_CB load_cb;
	ZQDBCalcModuleSave_CB save_cb;
	ZQDBCalcModuleCompile_CB compile_cb;
	//ZQDBCalcModuleApply_CB apply_cb;
};

typedef int (*fnZQDBCalcModuleStart)(ZQDB_CALC_MODULE_INF* inf);
typedef void (*fnZQDBCalcModuleStop)();

CALC_MODULE_API_EXPORT int ZQDBCalcModuleStart(ZQDB_CALC_MODULE_INF* inf);
CALC_MODULE_API_EXPORT void ZQDBCalcModuleStop();

#pragma pack(pop)

#if defined(__cplusplus)
}

#endif//

#endif//_H_ZQDB_CALC_MODULE_H_