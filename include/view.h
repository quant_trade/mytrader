#pragma once

#include <calcif.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <array>
#include <map>
#include <set>
#include <iostream>
#include <iomanip>
#include <memory>
#include <mutex>

#include <wxutil.h>
#include <XUtil/XXml.hpp>
#include <XUtil/XDateTime.hpp>

#include "wx/wxprec.h"

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#include "wx/mdi.h"
#endif

#include "wx/app.h"
#include "wx/config.h"
#include "wx/frame.h"
#include "wx/toolbar.h"
#include "wx/sizer.h"
#include "wx/menu.h"
#include "wx/wizard.h"
#include "wx/msgdlg.h"
#include "wx/dcbuffer.h"
#include "wx/colordlg.h"
#include "wx/wrapsizer.h"
#include "wx/image.h"
#include "wx/dcbuffer.h"
#include "wx/dcgraph.h"
#include "wx/overlay.h"
#include "wx/graphics.h"
#include "wx/filename.h"
#include "wx/metafile.h"
#include "wx/settings.h"
#if wxUSE_SVG
#include "wx/dcsvg.h"
#endif
#if wxUSE_POSTSCRIPT
#include "wx/dcps.h"
#endif
#include "wx/artprov.h"
#include "wx/ribbon/art.h"
#include "wx/dataview.h"
#include "wx/splash.h"
#include "wx/spinbutt.h"
#include "wx/spinctrl.h"
#include "wx/statline.h"
#include "wx/laywin.h"

#include "wx/choicebk.h"
#include "wx/listbook.h"
#include "wx/treebook.h"
#include "wx/notebook.h"
#include "wx/simplebook.h"
#include "wx/toolbook.h"
#include "wx/aui/auibook.h"

#include "wx/hyperlink.h"

// Main propertygrid header.
#include "wx/propgrid/propgrid.h"

// Extra property classes.
#include "wx/propgrid/advprops.h"

// This defines wxPropertyGridManager.
#include "wx/propgrid/manager.h"

#include "wx/richtext/richtextctrl.h"
#include "wx/richtext/richtextstyles.h"
#include "wx/richtext/richtextxml.h"
#include "wx/richtext/richtexthtml.h"
#include "wx/richtext/richtextformatdlg.h"
#include "wx/richtext/richtextsymboldlg.h"
#include "wx/richtext/richtextstyledlg.h"
#include "wx/richtext/richtextprint.h"
#include "wx/richtext/richtextimagedlg.h"

#include "wx/stc/stc.h"  // styled text control

#if wxUSE_BUSYINFO
#include "wx/busyinfo.h"
#endif // wxUSE_BUSYINFO

#if wxUSE_INFOBAR
#include "wx/infobar.h"
#endif // wxUSE_INFOBAR

#if wxUSE_RICHMSGDLG
#include "wx/richmsgdlg.h"
#endif // wxUSE_RICHMSGDLG

#include "wx/chartpanel.h"

#if wxUSE_PROGRESSDLG
#if wxUSE_STOPWATCH && wxUSE_LONGLONG
#include "wx/datetime.h"      // wxDateTime
#endif

#include "wx/progdlg.h"
#endif // wxUSE_PROGRESSDLG

// Standard DC supports drawing with alpha on OSX and GTK3.
#if defined(__WXOSX__) || defined(__WXGTK3__)
#define wxDRAWING_DC_SUPPORTS_ALPHA 1
#else
#define wxDRAWING_DC_SUPPORTS_ALPHA 0
#endif // __WXOSX__ || __WXGTK3__

//wxDECLARE_EVENT(ZQDB_CODE_PREV_EVENT, wxCommandEvent);
//wxDECLARE_EVENT(ZQDB_CODE_NEXT_EVENT, wxCommandEvent);

namespace zqdb {

	class TitleView;
	class MmpView;
	class TradeView;
	class InfoView;
	class TickView;

	#define MAX_BAR_SCALE	7

	class Rect : public wxRect
	{
		typedef wxRect Base;
	public:
		//using wxRect::wxRect;
		Rect() : Base() {}
		Rect(int xx, int yy, int ww, int hh) : Base(xx, yy, ww, hh) { }
		Rect(const wxPoint& topLeft, const wxPoint& bottomRight) : Base(topLeft, bottomRight) { }
		Rect(const wxPoint& pt, const wxSize& size) : Base(pt, size) { }
		Rect(const wxSize& size) : Base(size) { }
		Rect(const wxRect& o);

		Rect& Inflate(wxCoord left, wxCoord top, wxCoord right, wxCoord bottom);
		Rect& InflateX(wxCoord left, wxCoord right);
		Rect& InflateY(wxCoord top, wxCoord bottom);
		Rect& InflateLeft(wxCoord d);
		Rect& InflateRight(wxCoord d);
		Rect& InflateTop(wxCoord d);
		Rect& InflateBottom(wxCoord d);

		Rect& Deflate(wxCoord left, wxCoord top, wxCoord right, wxCoord bottom) {
			return Inflate(-left, -top, -right, -bottom);
		}
		Rect& DeflateX(wxCoord left, wxCoord right) {
			return InflateX(-left, -right);
		}
		Rect& DeflateY(wxCoord top, wxCoord bottom) {
			return InflateY(-top, -bottom);
		}
		Rect& DeflateLeft(wxCoord d) {
			return InflateLeft(-d);
		}
		Rect& DeflateRight(wxCoord d) {
			return InflateRight(-d);
		}
		Rect& DeflateTop(wxCoord d) {
			return InflateTop(-d);
		}
		Rect& DeflateBottom(wxCoord d) {
			return InflateBottom(-d);
		}
	};

	enum wxTriangle
	{
		wxTRIANGLE_NORMAL = 0x0000,
		//wxTRIANGLE_LARGE = 0x1000,
		wxTRIANGLE_MASK = 0x7000
	};
	void DrawTriangle(wxDC& dc, const Rect& rc, int flag = wxALIGN_CENTER | wxDOWN, Rect* rcBound = nullptr);

	wxString Status2wxString(char status);
	wxString UserStatus2wxString(char status);

	wxDECLARE_EVENT(ZQDB_TASK_EVENT, wxCommandEvent);

	class TaskID
	{
	public:
		TaskID();
		TaskID(size_t _delay);
		TaskID(const TaskID& o);
		TaskID& operator=(const TaskID& o);
		TaskID(TaskID&& o);
		TaskID& operator=(TaskID&& o);

		inline void reset() { id = 0, time = std::chrono::steady_clock::time_point(); }
		inline operator bool() const { return id != 0; }
		inline bool operator<(const TaskID &o) const
		{
			if (time < o.time) {
				return true;
			}
			else if (time > o.time) {
				return false;
			}
			return id < o.id;
		}

		size_t id;
		std::chrono::steady_clock::time_point time;
	};

	class TaskQue
	{
	public:
		void Push(const TaskID& key, std::function<void()> && task);
		void Push(std::function<void()> && task);
		void Remove(const TaskID& t);

		size_t Count();
		bool IsEmpty();

		bool Pop(std::function<void()>& task, ssize_t* dealy);
		void Clear();

	protected:
		bool IsActive(const TaskID& t, ssize_t* delay);

	private:
		std::map<TaskID, std::function<void()>> tasks_;
		std::queue<std::function<void()>> tasks_que_;
	};

	template<class TMutex = std::mutex>
	class TaskHandler : public wxTimer, public TaskQue
	{
		typedef TaskHandler<TMutex> This;
		typedef wxTimer Base;
	protected:
		TMutex mutex_;
	public:
		TaskHandler()
		{
			Bind(ZQDB_TASK_EVENT, &This::OnTaskNotify, this);
		}
		~TaskHandler()
		{
			ASSERT(TaskQue::IsEmpty());
			TaskQue::Clear();
		}

		inline TaskID Post(const size_t delay, std::function<void()> && task)
		{
			TaskID key(delay);
			std::lock_guard<TMutex> lock(mutex_);
			TaskQue::Push(key, std::move(task));
			if (!delay) {
				PostNotify();
			}
			else {
				PostTimer(delay);
			}
			return key;
		}

		inline void Post(std::function<void()> && task)
		{
			std::lock_guard<TMutex> lock(mutex_);
			InnerPost(std::move(task));
		}

		inline void Cancel(const TaskID& t)
		{
			std::unique_lock<TMutex> lock(mutex_);
			TaskQue::Remove(t);
		}

		inline void PostNotify()
		{
			if (wxThread::IsMain()) {
				const wxCommandEvent event(ZQDB_TASK_EVENT);
				wxPostEvent(this, event);
			}
			else {
				auto event = new wxCommandEvent(ZQDB_TASK_EVENT);
				wxQueueEvent(this, event);
			}
		}

		inline void PostTimer(size_t delay)
		{
			if (wxThread::IsMain()) {
				wxTimer::Start(delay, true);
			}
			else {
				InnerPost([this, delay]() {
					wxTimer::Start(delay, true);
				});
			}
		}

		void OnTaskNotify(wxCommandEvent& event)
		{
			DoTask();
		}

		virtual void Notify()
		{
			DoTask();
		}

	protected:
		//
		inline void InnerPost(std::function<void()> && task) {
			TaskQue::Push(std::move(task));
			PostNotify();
		}

		void DoTask()
		{
			std::unique_lock<TMutex> lock(mutex_);
			std::function<void()> task;
			size_t i = 0, j = TaskQue::Count();
			for (; i < j; i++) {
				ssize_t delay = 0;
				if (TaskQue::Pop(task, &delay)) {
					lock.unlock();
					task();
					lock.lock();
				}
				else {
					if (delay > 0) {
						PostTimer(delay);
					}
					break;
				}
			}
		}
	};

	template<class T, class TMutex = std::mutex>
	class TaskImplT
	{
	protected:
		TaskHandler<TMutex> task_handler_;
	public:
		//using Base::Base;

		inline void Clear()
		{
			task_handler_.Clear();
		}

		inline TaskID Post(const size_t delay, std::function<void()> && task)
		{
			return task_handler_.Post(delay, std::move(task));
		}

		inline void Post(std::function<void()> && task)
		{
			task_handler_.Post(std::move(task));
		}

		inline void Cancel(const TaskID& t)
		{
			task_handler_.Cancel(t);
		}
	};

	class SkinInfo
	{
	public:
		SkinInfo()
		{
		}
		~SkinInfo()
		{
			if (artProvider) {
				delete artProvider;
				artProvider = nullptr;
			}
		}

		// const wxColour& GetUpDownColor(int nUpDown);
		// const wxColour& GetUpDownLineColor(int nUpDown);
		// const wxPen& GetUpDownPen(int nUpDown);
		// const wxPen& GetUpDownLinePen(int nUpDown);
		// const wxBrush& GetUpDownBrush(int nUpDown);
		// const wxBrush& GetUpDownLineBrush(int nUpDown);

		//默认显示配置

		wxRibbonArtProvider* artProvider = nullptr;
		inline long GetArtFlags() const {
			return artProvider->GetFlags();
		}
		inline int GetArtMetric(int id)  const {
			return artProvider->GetMetric(id);
		}
		inline wxFont GetArtFont(int id)  const {
			return artProvider->GetFont(id);
		}
		inline wxColour GetArtColor(int id) const {
			return artProvider->GetColor(id);
		}
		inline void GetArtColourScheme(wxColour* primary,
			wxColour* secondary,
			wxColour* tertiary) const {
			return artProvider->GetColourScheme(primary, secondary, tertiary);
		}

		//鼠标
		wxCursor curDragLeftRight; //左右/西东
		wxCursor curDragUpDown; //上下/北南
		wxCursor curDrawLine; //划线

		//颜色
		wxColour crBackgnd; //背景
		wxColour crTabSelBackgnd; //标签选中背景
		wxColour crRptTitleBakcgnd;	//报表标题背景
		wxColour crRptSelBackgnd; //报表选中背景

		wxColour crTitle; //标题
		wxColour crText; //文字
		wxColour crRising; //上涨
		wxColour crFalling; //下跌
		wxColour crCommodityCode; //代码
		wxColour crCommodityName; //名称
		wxColour crAmount; //额
		wxColour crVolume; //量
		wxColour crTabSel; //标签选中

		wxColour crLine; //线
		wxColour crAverageLine; //均线
		wxColour crDrawLine; //画线
		wxColour crXYLine; //X、Y分隔线
		wxColour crXText; //X坐标文字
		wxColour crYText; //Y坐标文字
		wxColour crCrossCursor; //十字游标	
		wxColour crRptLine; //报表线
		wxColour crRisingLine; //上涨线
		wxColour crFallingLine; //下跌线
		wxColour crRefline; //参考线
		std::array<wxColour,8> crILine;	//指标线

		template<class _Ty>
		inline wxColour GetColor(const _Ty& val) {
			if (val > 0.) {
				return crRising;
			}
			else if (val < 0.) {
				return crFalling;
			}
			return crText;
		}

		template<class _Ty>
		inline wxColour GetLineColor(const _Ty& val) {
			if (val > 0.) {
				return crRisingLine;
			}
			else if (val < 0.) {
				return crFallingLine;
			}
			return crLine;
		}

		//画笔
		wxPen penTitle;
		wxPen penLine;
		wxPen penRisingLine; //上涨线
		wxPen penFallingLine; //下跌线
		wxPen penAverageLine; //均线
		wxPen penDrawLine; //画线
		wxPen penXYLine; //X、Y分隔线
		wxPen penCrossCursor;
		wxPen penYCrossCursor;

		template<class _Ty>
		inline wxPen GetPen(const _Ty& val) {
			if (val > 0.) {
				return penRisingLine;
			}
			else if (val < 0.) {
				return penFallingLine;
			}
			return penLine;
		}

		//画刷
		wxBrush brush;
		wxBrush brushRising;
		wxBrush brushFalling;
		wxBrush brushDrawLine;
		wxBrush brushCrossCursor;
		wxBrush brushNull;

		template<class _Ty>
		inline wxBrush GetBursh(const _Ty& val) {
			if (val > 0.) {
				return brushRising;
			}
			else if (val < 0.) {
				return brushFalling;
			}
			return brush;
		}

		//图片

		//字体
		wxFont fontTitle;
		wxFont fontText;
		wxFont fontRptTitle;
		wxFont fontRptText;
		wxFont fontXText;
		wxFont fontYText;

		//字体高宽
		wxSize xyTitle;
		wxSize xyText;
		wxSize xyRptTitle;
		wxSize xyRptText;
		wxSize xyXText;
		wxSize xyYText;
		wxSize xySpace;

		//窗体高宽（相对于水平或者垂直布局）
		wxSize xyTabCtrl; //默认标签水平高度，或者垂直时宽度
		wxSize xyScrollBar; //默认滚动条水平高度，或者垂直时宽度
		wxSize xyWndIndicator; //默认指标窗口水平高度，或者垂直时宽度
		wxSize xyInfoIndicator; //默认指标信息栏水平右侧信息栏宽度和顶部标题栏高度，或者垂直时（忽略）
		wxSize xyCoordinate; //默认时间轴坐标水平高度，或者垂直时（忽略）

		std::array<size_t, MAX_BAR_SCALE> nBarWidth; //K线柱宽度调整[单位像素]
		std::array<size_t, MAX_BAR_SCALE> nBarSpace; //K线柱间隙调整[单位像素]
		
		//std::map<mdb::Field, wxString> field_names_;	//多语言
		//void SetFieldName(const mdb::Field& field, const wxString& name);
		//virtual const wxString& GetFieldName(const mdb::Field& field);
	};

	template<class T, class TSkinInfo = SkinInfo>
	class SkinMap
	{
	protected:
		std::shared_ptr<TSkinInfo> skin_info_ptr_;

	public:
		SkinMap()
		{
			
		}

		void Clear()
		{
			skin_info_ptr_.reset();
		}
		inline bool IsDispOk() const {
			return skin_info_ptr_ ? true : false;
		}
		const std::shared_ptr<TSkinInfo>& GetSkinInfo() const
		{
			return skin_info_ptr_;
		}
		void SetSkinInfo(const std::shared_ptr<TSkinInfo>& skin_info_ptr)
		{
			T* pT = static_cast<T*>(this);
			skin_info_ptr_ = skin_info_ptr;
			pT->OnSkinInfoChanged();
		}
		void OnSkinInfoChanged()
		{
		}
	};

	template<class Ty>
	class ObjectRefDataT : public wxObjectRefData
	{
	public:
		Ty data_;

		ObjectRefDataT(const Ty& data) :data_(data) 
		{
		}

		~ObjectRefDataT() 
		{
		}

		inline Ty& GetData() { return data_; }
	};

	/*class Menu : public wxMenu
	{
	public:
		using wxMenu::wxMenu;

		template<class Ty = void*>
		void SetMenuItemClientData(wxMenuItem* item, const Ty& data)
		{
			item->SetRefData(new ObjectRefDataT<Ty>(data));
		}

		template<class Ty = void*>
		bool GetMenuItemClientData(wxMenuItem* item, Ty& data)
		{
			auto refdata = dynamic_cast<ObjectRefDataT<Ty>*>(menu_item->GetRefData());
			if (!refdata) {
				return false;
			}
			data = refdata->data_;
			return true;
		}
	};*/

	template<class Ty = void*>
	void SetMenuItemClientData(wxMenuItem* item, const Ty& data)
	{
		item->SetRefData(new ObjectRefDataT<Ty>(data));
	}

	template<class Ty>
	bool GetMenuItemClientData(Ty& data, wxCommandEvent& evt)
	{
		wxMenu* menu = wxDynamicCast(evt.GetEventObject(), wxMenu);
		if (!menu) {
			return false;
		}
		wxMenuItem* menu_item = menu->FindItem(evt.GetId());
		if (!menu_item) {
			return false;
		}
		auto refdata = dynamic_cast<ObjectRefDataT<Ty>*>(menu_item->GetRefData());
		if (!refdata) {
			return false;
		}
		data = refdata->data_;
		return true;
	}

	class ContainerInfo
	{
	public:
		int type_;
		wxString Key_;
		long Extra_;
		void* data_;
	};

	class TechContainerInfo : public ContainerInfo
	{
	public:
		bool read_only_ = false;
		HZQDB h_ = nullptr;
		HZQDB user_ = nullptr;
	public:
		TechContainerInfo(bool read_only = false) : read_only_(read_only)
		{
		}

		bool IsReadOnly() { return read_only_; }
		bool IsSame(HZQDB h, HZQDB user = nullptr) {
			if (h_ == h && user_ == user) {
				return true;
			}
			return false;
		}
		HZQDB Get() { return h_; }
		HZQDB GetUser() { return user_; }
		void Set(HZQDB h) { h_ = h; }
		void SetUser(HZQDB user) { user_ = user; }
	};

	class FuncContainerInfo : public ContainerInfo
	{
	public:
		bool new_flag_ = false;
		HZQDB hmodule_ = nullptr;
		CALC_LANG_TYPE lang_;
		CALC_TYPE type_;
		wxString name_;
		wxString source_;
	public:
		FuncContainerInfo(CALC_TYPE type, const wxString& name, CALC_LANG_TYPE lang)
		{
			New(type, name, lang);
		}
		FuncContainerInfo(zqdb::Calc::Func& func)
		{
			Open(func);
		}

		void New(CALC_TYPE type, const wxString& name, CALC_LANG_TYPE lang)
		{
			new_flag_ = true;
			lang_ = lang;
			type_ = type;
			name_ = name;
			zqdb::Calc::AllModule allmodule;
			for (auto it = allmodule.rbegin(); it != allmodule.rend(); ++it)
			{
				if (ZQDBCalcModuleGetLang(*it) == lang_) {
					hmodule_ = *it;
					break;
				}
			}
			wxASSERT(hmodule_);
			ZQDBCalcModuleNewFunc(hmodule_, type_, name_.c_str(), [](const char* src, void* data) {
				((FuncContainerInfo*)data)->SetSource(utf2wxString(src));
			}, this);
		}

		void Open(zqdb::Calc::Func& func)
		{
			new_flag_ = false;
			lang_ = func.GetCalcLang();
			type_ = func.GetCalcType();
			name_ = func.GetCalcName();
			zqdb::Calc::AllModule allmodule;
			for (auto it = allmodule.rbegin(); it != allmodule.rend(); ++it)
			{
				if (ZQDBCalcModuleGetLang(*it) == lang_) {
					hmodule_ = *it;
					break;
				}
			}
			ZQDBCalcModuleLoadFunc(hmodule_, type_, name_.c_str(), [](const char* src, void* data) {
				((FuncContainerInfo*)data)->SetSource(utf2wxString(src));
			}, this);
		}

		inline void SetSource(const wxString& source)
		{
			source_ = source;
		}
		inline const wxString& GetSource() { return source_; }
	};

	template<class T, class TInfo = ContainerInfo>
	class ContainerMap
	{
	protected:
		//
		std::shared_ptr<TInfo> info_ptr_;

	public:
		void OnInfoChanged()
		{
		}

		const std::shared_ptr<TInfo>& GetInfo() const { return info_ptr_; }
		void SetInfo(const std::shared_ptr<TInfo>& info_ptr) {
			T* pT = static_cast<T*>(this);
			info_ptr_ = info_ptr;
			pT->OnInfoChanged();
		}

		bool IsOk() { return info_ptr_ != nullptr; }
	};

	template<class T, class TInfo = TechContainerInfo>
	class TechContainerMap : public ContainerMap<T, TInfo>
	{
	public:
		bool IsReadOnly() { return info_ptr_->IsReadOnly(); }
		bool IsSame(HZQDB h) { return info_ptr_->IsSame(h); }
		HZQDB Get() { return info_ptr_->Get(); }
		HZQDB GetUser() { return info_ptr_->GetUser(); }
		void OnHandleChanged() { }
		void OnUserChanged() { }
		void Set(HZQDB h) {
			T* pT = static_cast<T*>(this);
			if (h != info_ptr_->Get()) {
				info_ptr_->Set(h);
				pT->OnHandleChanged();
			}
		}
		void SetUser(HZQDB user)
		{
			T* pT = static_cast<T*>(this);
			if (user != info_ptr_->GetUser()) {
				info_ptr_->SetUser(user);
				pT->OnUserChanged();
			}
		}
	};

	template<class T, class TInfo = FuncContainerInfo>
	class FuncContainerMap : public ContainerMap<T, TInfo>
	{
	public:
		void OnInfoChanged()
		{
			T* pT = static_cast<T*>(this);
			pT->OnFuncChanged();
		}
		void OnFuncChanged() { }
		void New(CALC_TYPE type, const wxString& name) {
			T* pT = static_cast<T*>(this);
			info_ptr_->New(type, name); 
			pT->OnFuncChanged();
		}
		void Open(zqdb::Calc::Func& func) {
			T* pT = static_cast<T*>(this);
			info_ptr_->Open(func);
			pT->OnFuncChanged();
		}

		inline bool IsNew() { return info_ptr_->new_flag_; }
		inline HZQDB GetCalcModule() { return info_ptr_->hmodule_; }
		inline CALC_LANG_TYPE GetCalcLang() { return info_ptr_->lang_; }
		inline CALC_TYPE GetCalcType() { return info_ptr_->type_; }
		inline const wxString& GetCalcName() { return info_ptr_->name_; }
		inline const wxString& GetCalcSource() { return info_ptr_->GetSource(); }
	};

	class StrategyInfo
	{
	public:
		std::string name;
		//HZQDB input = nullptr;
		//HZQDB data = nullptr;
		zqdb::Calc::Strategy strategy;

		StrategyInfo(HZQDB func, HZQDB input, HZQDB data, PERIODTYPE cycle, size_t cycleex, HZQDB* user, size_t user_count, size_t flags = 0);
		~StrategyInfo();

		bool IsRun();
		bool Start();
		void Puase();
		void Stop();
	};

	enum OrderFlags
	{
		ORDER_FLAG_NONE = 0,
		ORDER_FLAG_SHOW_SUCCESS_TIPS = 0X01,
		ORDER_FLAG_SHOW_FAILURE_TIPS = 0X02,
		ORDER_FLAGS_DEFAULT = ORDER_FLAG_SHOW_SUCCESS_TIPS | ORDER_FLAG_SHOW_FAILURE_TIPS,
	};

	class Module
		: public ObjectT<tagZQDBModuleInfo>
		, public NotifyMap<Module>
	{
		typedef ObjectT<tagZQDBModuleInfo> Base;
		typedef NotifyMap<Module> NotifyBase;
	public:
		Module(HZQDB h);
		virtual ~Module();

		//获取信息
		virtual wxString GetUserInfo(HZQDB user) { return wxEmptyString; }

		virtual double OrderDefaultVolume(HZQDB user, HZQDB code) { return ZQDBCalcDefaultTradeVolume(code, 1.); }
		
		//请求动作
		virtual HZQDB ReqTestUser(double amount) { return 0; }
		virtual void ReqTestUserUpdate(HZQDB user, uint32_t date, uint32_t time) { }

		virtual int ReqModifyPassword(HZQDB user, const char* old_pwd, const char* new_pwd, HNMSG* rsp, size_t timeout = 3000, size_t flags = 0) { return 0; }
		virtual int ReqModifyAccountPassword(HZQDB account, const char* old_pwd, const char* new_pwd, HNMSG* rsp, size_t timeout = 3000, size_t flags = 0) { return 0; }

		virtual int OrderSend(HZQDB user, HZQDB code, char direction, char offset, char type, double volume, double price, HNMSG* rsp, size_t timeout = 3000, size_t flags = 0) { return 0; }
		//, int slippage, double stoploss, double takeprofit,
		//const char* comment, int magic, unsigned long date, unsigned long time);
		virtual int OrderCancel(HZQDB user, HZQDB order, HNMSG* rsp = nullptr, size_t timeout = 0, size_t flags = 0) { return 0; }
		virtual int OrderClose(HZQDB user, HZQDB position, char type, double volume, double price, HNMSG* rsp, size_t timeout = 3000, size_t flags = 0) { return 0; }

		virtual int ReqQryMaxOrderVolume(HZQDB user, HZQDB code, char direction, char offset, char type, HNMSG* rsp, size_t timeout = 3000, size_t flags = 0) { return 0; }

		virtual void OnNotifyStatus(HZQDB h) { }
		virtual void OnNotifyAdd(HZQDB h) { }
		virtual void OnNotifyRemove(HZQDB h) { }
		virtual void OnNotifyUpdate(HZQDB h) { }

		virtual int OnNetMsg(zqdb::Msg& msg) { return 0; }
	};

	class App : public wxApp
		, public TaskImplT<App>
	{
	public:
		typedef TaskImplT<App> TaskBase;
	protected:
		std::vector<std::shared_ptr<Module>> modules_;
		std::map<int, void*> menu_map_;
		std::map<std::string, std::shared_ptr<StrategyInfo>> all_strategy_;
	public:

		bool OnInit() wxOVERRIDE; 
		void FinalRelease();
		int OnExit() wxOVERRIDE;

		void Clear();

		const std::vector<std::shared_ptr<Module>>& AllModule() { return modules_; }
		std::shared_ptr<Module> FindModule(HZQDB h);
		void BroadcastModule(const std::function<bool(std::shared_ptr<Module>)>& f);
		void RemoveAllModule();

		virtual void Goto(HZQDB h, wxWindow* top = nullptr) = 0;

		void ClearMenuMap();
		void SetMenuData(int id, void* data);
		void* GetMenuData(int id);

		virtual int ShowCalcFuncDlg(wxWindow* parent, CALC_TYPE type, const char* name) { return wxID_CANCEL; }
		virtual void ShowCalcFrame(std::shared_ptr<zqdb::FuncContainerInfo> info_ptr) {}

		std::shared_ptr<StrategyInfo> FindStrategy(const std::string& name);
		std::shared_ptr<StrategyInfo> StartStrategy(HZQDB hfunc, HZQDB input, HZQDB data, PERIODTYPE cycle, size_t cycleex, HZQDB* user, size_t user_count, size_t flags = 0);
		void StopStrategy(std::shared_ptr<StrategyInfo> sp);
		void StopAllStrategy();

		int OrderSend(HZQDB user, HZQDB code, char direction, char offset, char type, double volume, double price, HNMSG* rsp, size_t timeout = 3000, size_t flags = 0);
		int OrderCancel(HZQDB user, HZQDB order, HNMSG* rsp = nullptr, size_t timeout = 0, size_t flags = 0);
		int OrderClose(HZQDB user, HZQDB position, char type, double volume, double price, HNMSG* rsp, size_t timeout = 3000, size_t flags = 0);

		int SendOrder(HZQDB user, HZQDB code, char direction, char offset, char type, double volume, double price, size_t flags = ORDER_FLAGS_DEFAULT);
		int CancelOrder(HZQDB user, HZQDB order, size_t flags = ORDER_FLAGS_DEFAULT);
		int CloseOrder(HZQDB user, HZQDB position, char type, double volume, double price, size_t flags = ORDER_FLAGS_DEFAULT);

		virtual void ShowTips(const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING) {}
	};
	template<class Ty = App>
	inline Ty& GetApp() { return *(Ty*)wxTheApp; }

	///
	//可以根据XML配置显示HZQDB信息
	///

	class HZQDBModel 
		: public wxDataViewVirtualListModel
		, public HandleMap<HZQDBModel>
	{
	public:
		struct RowInfo {
			wxString name;
			MDB_FIELD field;
			int precision;
			RowInfo() : field{ 0 }, precision(0) {}
		};
	protected:
		HZQDB module_;
		std::vector<RowInfo> row_items_;
	public:
		HZQDBModel(HZQDB module, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);

		void OnHandleChanged();

		// implementation of base class virtuals to define model

		virtual unsigned int GetColumnCount() const wxOVERRIDE
		{
			return 2;
		}

		virtual wxString GetColumnType(unsigned int col) const wxOVERRIDE
		{
			return "string";
		}

		virtual void GetValueByRow(wxVariant &variant,
			unsigned int row, unsigned int col) const wxOVERRIDE;
		virtual bool GetAttrByRow(unsigned int row, unsigned int col,
			wxDataViewItemAttr &attr) const wxOVERRIDE;
		virtual bool SetValueByRow(const wxVariant &variant,
			unsigned int row, unsigned int col) wxOVERRIDE;
	};

	///
	//可以根据XML配置显示HZQDB列表信息
	///

	class HZQDBListModel : public wxDataViewVirtualListModel
	{
	public:
		struct ColInfo {
			wxString name;
			MDB_FIELD field;
			int precision;
			ColInfo() :field{ 0 }, precision(0){}
		};
	protected:
		HZQDB module_;
		std::string table_;
		std::vector<ColInfo> col_items_;
		std::vector<HZQDB> val_items_;
	public:
		HZQDBListModel(HZQDB module, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);

		const std::vector<ColInfo>& GetColInfo() { return col_items_; }
		void Clear();
		void Show(const std::vector<HZQDB>& h_list);
		HZQDB GetData(const wxDataViewItem& item);

		// implementation of base class virtuals to define model

		virtual unsigned int GetColumnCount() const wxOVERRIDE
		{
			return col_items_.size();
		}

		virtual wxString GetColumnType(unsigned int col) const wxOVERRIDE
		{
			return "string";
		}

		virtual void GetValueByRow(wxVariant &variant,
			unsigned int row, unsigned int col) const wxOVERRIDE;
		virtual bool GetAttrByRow(unsigned int row, unsigned int col,
			wxDataViewItemAttr &attr) const wxOVERRIDE;
		virtual bool SetValueByRow(const wxVariant &variant,
			unsigned int row, unsigned int col) wxOVERRIDE;
	};

	class HMTABLEListModel : public wxDataViewVirtualListModel
	{
	public:
		struct ColInfo {
			wxString name;
			MDB_FIELD field;
			int precision;
			ColInfo() :field{ 0 }, precision(0){}
		};
	protected:
		std::string table_;
		std::vector<ColInfo> col_items_;
		HMDB hdb_ = nullptr;
		HMTABLE htb_ = nullptr;
	public:
		HMTABLEListModel(const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);

		const std::vector<ColInfo>& GetColInfo() { return col_items_; }
		void Clear();
		void Show(HMDB hdb, HMTABLE htb);

		// implementation of base class virtuals to define model

		virtual unsigned int GetColumnCount() const wxOVERRIDE
		{
			return col_items_.size();
		}

		virtual wxString GetColumnType(unsigned int col) const wxOVERRIDE
		{
			return "string";
		}

		virtual void GetValueByRow(wxVariant &variant,
			unsigned int row, unsigned int col) const wxOVERRIDE;
		virtual bool GetAttrByRow(unsigned int row, unsigned int col,
			wxDataViewItemAttr &attr) const wxOVERRIDE;
		virtual bool SetValueByRow(const wxVariant &variant,
			unsigned int row, unsigned int col) wxOVERRIDE;
	};

	//////////////////////////////////////////////////////////////////////////

	enum ViewState
	{
		VIEW_STATE_NORMAL = 0,
		VIEW_STATE_HOVERED,
		VIEW_STATE_ACTIVE,
		VIEW_STATE_DISABLED
	};

	///

	template<class T>
	class CycleMap
	{
		typedef CycleMap<T> This;
	protected:
		PERIODTYPE cycle_ = CYC_1MIN;
		size_t cycleex_ = 0;

	public:
		void OnCycleChanged()
		{

		}

		size_t IsTrend() 
		{
			if (cycle_ == CYC_1MIN) {
				return cycleex_;
			}
			return 0;
		}

		const PERIODTYPE GetCycle() const
		{
			return cycle_;
		}
		const size_t GetCycleEx() const
		{
			return cycleex_;
		}
		void SetCycle(const PERIODTYPE cycle, size_t cycleex = 0)
		{
			T* pT = static_cast<T*>(this);
			if (cycle_ != cycle || cycleex_ != cycleex) {
				cycle_ = cycle;
				cycleex_ = cycleex;
				pT->OnCycleChanged();
			}
		}
	};

	///

	template<class T>
	class CalcDataMap
	{
		typedef CalcDataMap<T> This;
	protected:
		std::shared_ptr<zqdb::Calc::Data> calc_data_ptr_;

	public:
		void OnCalcDataChanged()
		{

		}
		void OnCalcDataUpdated()
		{

		}

		void Clear()
		{
			calc_data_ptr_.reset();
		}
		inline bool IsDispOk() const { return calc_data_ptr_ != nullptr; }
		const std::shared_ptr<zqdb::Calc::Data>& GetCalcData() const
		{
			return calc_data_ptr_;
		}
		void SetCalcData(const std::shared_ptr<zqdb::Calc::Data>& calc_data)
		{
			T* pT = static_cast<T*>(this);
			calc_data_ptr_ = calc_data;
			pT->OnCalcDataChanged();
		}
		void UpdateCalcData()
		{
			T* pT = static_cast<T*>(this);
			pT->OnCalcDataUpdated();
		}

		const HZQDB GetCode()
		{
			return calc_data_ptr_->GetCode();
		}

		const PERIODTYPE GetCycle() const
		{
			return calc_data_ptr_->GetCycle();
		}
		const size_t GetCycleEx() const
		{
			return calc_data_ptr_->GetCycleEx();
		}

		/*ENUM_DWTYPE GetDWType(unsigned long* pDWDate)
		{
		if (pDWDate) {
		*pDWDate = calc_info_ptr_->DWDate;
		}
		return calc_info_ptr_->DWType;
		}*/

		size_t GetBarCount() const
		{
			return calc_data_ptr_->GetDataCount();
		}

		bool IsBarPosOk(int nPos) const
		{
			if (nPos >= 0 && nPos<GetBarCount()) {
				return true;
			}
			return false;
		}
	};

	template<class T>
	class ModuleMap : public HandleMap<T>
	{
		typedef ModuleMap<T> This;
		typedef HandleMap<T> Base;
	protected:
		std::shared_ptr<Module> module_;

	public:
		void OnModuleChanged()
		{

		}

		void Clear()
		{
			module_.reset();
		}
		bool IsOk() const { return module_ && Base::IsOk(); }
		std::shared_ptr<Module> GetModule() const
		{
			return module_;
		}
		void SetHandle(HZQDB h)
		{
			T* pT = static_cast<T*>(this);
			if (h_ != h) {
				auto module = GetApp().FindModule(h);
				if (module_ != module) {
					module_ = module;
					pT->OnModuleChanged();
				}
				Base::SetHandle(h);
			}
		}
	};

	template<class T>
	class UserModuleMap : public UserMap<T>
	{
		typedef UserModuleMap<T> This;
		typedef UserMap<T> Base;
	protected:
		std::shared_ptr<Module> user_module_;

	public:
		void OnUserModuleChanged()
		{

		}

		void Clear()
		{
			user_module_.reset();
		}
		bool IsOk() const { return user_module_ && Base::IsOk(); }
		std::shared_ptr<Module> GetUserModule() const
		{
			return user_module_;
		}
		void SetUser(HZQDB user)
		{
			T* pT = static_cast<T*>(this);
			if (user_ != user) {
				auto user_module = GetApp().FindModule(user);
				if (user_module_ != user_module) {
					user_module_ = user_module;
					pT->OnUserModuleChanged();
				}
				Base::SetUser(user);
			}
		}
	};

	template<class T, class TBase = wxPanel>
	class BaseViewT
		: public TBase
		, public SkinMap<T>
		, public ModuleMap<T>
		, public NotifyMap<T>
		//, public MsgMap<T>
	{
		typedef BaseViewT<T, TBase> This;
	public:
		typedef TBase Base;
		typedef SkinMap<T> SkinBase;
		typedef ModuleMap<T> HandleBase;
		typedef NotifyMap<T> NotifyBase;
		//typedef MsgMap<T> MsgBase;
	protected:
		//int orient_ = 0;
	public:
		BaseViewT(wxWindow* parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING) 
			: Base(parent, wxID_ANY)
		{
			//T* pT = static_cast<T*>(this);
			//Bind(wxEVT_ERASE_BACKGROUND, &This::OnErase, this);
			//Bind(wxEVT_PAINT, &This::OnPaint, this);
		}

		/*void OnOrientChanged()
		{
			Refresh();
		}

		void SetOrient(int orient)
		{
			T* pT = static_cast<T*>(this);
			if (orient != orient_) {
				orient_ = orient;
				pT->OnOrientChanged();
			}
		}
		int GetOrient() { return orient_; }*/

		wxString FormatValue(double value) {
			if (IsInvalidValue(value)) {
				return wxT("——");
			}
			else {
				if (value > 1000000000000) {
					return wxString::Format(wxT("%.2f万亿"), value / 1000000000000);
				}
				else if (value > 100000000) {
					return wxString::Format(wxT("%.2f亿"), value / 100000000);
				}
				else if (value > 10000) {
					return wxString::Format(wxT("%.2f万"), value / 10000);
				}
				return wxString::Format(wxT("%.2f"), value);
			}
		}

		wxString FormatPrice(double value) {
			return IsInvalidValue(value) ? wxT("——") : wxString::Format(wxT("%.*f"), (int)ZQDBGetPriceDigits(h_), value);
		}

		wxString FormatVolume(double value, const wxString& def = wxEmptyString) {
			if (IsInvalidValue(value)) {
				return def;
			}
			else {
				zqdb::ObjectT<tagProductInfo> product(ZQDBGetParent(h_));
				auto unit = product->Type == PRODUCT_TYPE_Index ? 1 : product->TradingUnit;
				if (unit == 0) {
					return def;
				}
				value /= unit;
				if (value > 100000000) {
					return wxString::Format(wxT("%.2f亿"), value / 100000000);
				}
				else if (value > 10000) {
					return wxString::Format(wxT("%.2f万"), value / 10000);
				}
				return wxString::Format(wxT("%.0f"), value);
			}
		}

		wxString FormatVoldif(double value) {
			if (IsZeroValue(value)) {
				return wxEmptyString;
			}
			zqdb::ObjectT<tagProductInfo> product(ZQDBGetParent(h_));
			auto unit = product->Type == PRODUCT_TYPE_Index ? 1 : product->TradingUnit;
			if (unit == 0) {
				return wxEmptyString;
			}
			value /= unit;
			return wxString::Format(wxT("%+.0f"), value);
		}

		inline wxString FormatAmount(double value) {
			return FormatValue(value);
		}

		void OnSkinInfoChanged()
		{
			SetBackgroundColour(skin_info_ptr_->crBackgnd);
			Refresh();
		}

		void OnHandleChanged()
		{
			
		}

		void OnNotifyStatus(HZQDB h) {
			if (h == h_) {
				Refresh();
			}
		}
		void OnNotifyAdd(HZQDB h) { }
		void OnNotifyRemove(HZQDB h) { }
		void OnNotifyUpdate(HZQDB h)
		{
			if (h == h_) {
				Refresh();
			}
		}

		bool IsDispOk() const { return skin_info_ptr_ && h_; }

		Rect GetClientRect() const { return Base::GetClientRect(); }

	protected:
		wxSize best_size_;
		virtual wxSize DoGetBestSize() const { 
			if(best_size_.x || best_size_.y)
				return best_size_; 
			return Base::DoGetBestSize();
		}
	};

	class TitleView 
		: public BaseViewT<TitleView>
	{
		typedef TitleView This;
	public:
		typedef BaseViewT<TitleView> Base;
	protected:
	public:
		TitleView(wxWindow* parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);
		virtual ~TitleView();

		void OnSkinInfoChanged();

	protected:
		void CalcBestSize();

		void Draw(wxDC& dc);
		void OnErase(wxEraseEvent &event);
		void OnPaint(wxPaintEvent &event);

		wxDECLARE_EVENT_TABLE();
	};

	class MmpView
		: public BaseViewT<MmpView>
	{
		typedef MmpView This;
	public:
		typedef BaseViewT<MmpView> Base;
	protected:
		size_t level_ = 1;
		struct LevelInfo
		{
			double price = ZQDB_INVALID_VALUE;
			double volume = 0;
			double voldif = 0;
		};
		std::array<LevelInfo, 5> ask_infos_; //卖
		std::array<LevelInfo, 5> bid_infos_; //买
		bool valid_ = false;
		void AdjustLevel();
	public:
		MmpView(wxWindow* parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);
		virtual ~MmpView();

		size_t GetLevel() { return level_; }

		void OnSkinInfoChanged();
		void OnHandleChanged(); 

		void OnNotifyUpdate(HZQDB h);

	protected:
		void CalcBestSize();

		void Draw(wxDC& dc);

		void OnErase(wxEraseEvent &event);
		void OnPaint(wxPaintEvent &event);

		wxDECLARE_EVENT_TABLE();
	};

	class TradeView
		: public BaseViewT<TradeView>
		, public UserMap<TradeView>
	{
		typedef TradeView This;
	public:
		typedef BaseViewT<TradeView> Base;
		typedef UserMap<TradeView> UserBase;
	protected:
		wxButton* btn_price_decr_ = nullptr;
		wxTextCtrl* text_price_ = nullptr; //限价时需要输入价格
		wxButton* btn_price_incr_ = nullptr;
		wxButton* btn_volume_decr_ = nullptr;
		wxTextCtrl* text_volume_ = nullptr; //需要输入量
		wxButton* btn_volume_incr_ = nullptr;
		wxButton* btn_buy_ = nullptr;
		wxButton* btn_close_ = nullptr;
		wxButton* btn_sell_ = nullptr;
		Rect rcAskPrice; //卖一
		Rect rcPrice; //价
		Rect rcVolume; //量
		Rect rcBidPrice; //买一
		Rect rcBuyOpen; //买开
		Rect rcSellOpen; //卖开
		Rect rcBuyClose; //买平
		Rect rcSellClose; //卖平
	public:
		TradeView(wxWindow* parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);
		virtual ~TradeView();

		void OnSkinInfoChanged();

		void OnNotifyUpdate(HZQDB h);

		bool IsDispOk() const { return Base::IsDispOk() && UserBase::IsOk(); }

	protected:
		void CalcBestSize();
		//virtual wxSize DoGetBestSize() const;

		//0:normal,1:hover,2:click
		void Draw(wxDC& dc, int state = 0);

		void OnSize(wxSizeEvent &event);
		void OnErase(wxEraseEvent &event);
		void OnPaint(wxPaintEvent &event);
		void OnMouse(wxMouseEvent &event);
		void OnBtnBuy(wxCommandEvent &event);

		wxDECLARE_EVENT_TABLE();
	};

	class InfoView
		: public BaseViewT<InfoView>
	{
		typedef InfoView This;
	public:
		typedef BaseViewT<InfoView> Base;
		//特殊FIELD标识
		enum {
			FIELD_ID_MDB = 0, //MDB_FIELD
			FIELD_ID_BREAK = 1, //换行
			FIELD_ID_BREAKLINE = 2, //换行且显示换行线
		};
	protected:
		struct FieldInfo {
			wxString name;
			size_t id;
			MDB_FIELD field;
			int precision;
			FieldInfo() :id(FIELD_ID_MDB), field{ 0 }, precision(0){}
		};
		std::map<char,std::vector<FieldInfo>> fields_;
		char type_ = -1;
		size_t max_col_ = 0;
	public:
		InfoView(wxWindow* parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);
		virtual ~InfoView();

		void OnSkinInfoChanged();
		void OnHandleChanged();

		void OnNotifyStatus(HZQDB h);
	protected:
		void CalcBestSize();

		void Draw(wxDC& dc);

		void OnSize(wxSizeEvent &event);
		void OnErase(wxEraseEvent &event);
		void OnPaint(wxPaintEvent &event);
		void OnMouse(wxMouseEvent &event);

		wxDECLARE_EVENT_TABLE();
	};

	class TickView
		: public BaseViewT<TickView>
		, public CalcDataMap<TickView>
	{
		typedef TickView This;
	public:
		typedef BaseViewT<TickView> Base;
		typedef CalcDataMap<TickView> CalcDataBase;
	protected:
	public:
		TickView(wxWindow* parent, const char* xml = nullptr, size_t xmlflag = XUtil::XML_FLAG_JSON_STRING);
		virtual ~TickView();

		inline bool IsDispOk() const {
			return Base::IsDispOk() && CalcDataBase::IsDispOk();
		}

		void OnSkinInfoChanged();
		void OnHandleChanged();

		void OnNotifyUpdate(HZQDB h);

	protected:
		void CalcBestSize();

		void Draw(wxDC& dc);

		void OnSize(wxSizeEvent &event);
		void OnErase(wxEraseEvent &event);
		void OnPaint(wxPaintEvent &event);

		wxDECLARE_EVENT_TABLE();
	};

	wxDECLARE_EVENT(EVT_SPLITTER_CHANGE_EVENT, wxCommandEvent);

	class SplitterSizer : public wxBoxSizer, public wxEvtHandler
	{
		typedef wxBoxSizer Base;
	public:
		//using Base::Base;
		SplitterSizer(wxWindow* window, int orient);
		~SplitterSizer();

		wxSizerItem* AddSplitter(int size = 1);
		wxSizerItem* InsertSplitter(size_t index, int size = 1);
		void RemoveSplitter(size_t index);

		wxSizerItem* GetItemByWindow(const wxWindow* window, size_t* index = nullptr);
		wxSizerItem* GetItemByPoint(const wxPoint& pt, size_t* index = nullptr, bool window_only = false);
		inline wxSizerItem* GetWindowByPoint(const wxPoint& pt, size_t* index = nullptr) {
			return GetItemByPoint(pt, index, true);
		}
		bool IsSplitter(wxSizerItem* item);

	protected:
		void OnDestroy(wxWindowDestroyEvent &event);
		void OnSetCursor(wxSetCursorEvent &event);
		void OnMouseEnter(wxMouseEvent &event);
		void OnMouseLeave(wxMouseEvent &event);
		void OnMouseMove(wxMouseEvent &event);
		void OnMouseDown(wxMouseEvent &event);
		void OnMouseUp(wxMouseEvent &event);

		wxWindow* window_ = nullptr;
		std::vector<wxSizerItem*> splitters_;
		wxOverlay overlay_;
		wxPoint pt_capture_;
		wxSizerItem* item_capture_ = nullptr;
		size_t index_capture_ = 0;
	};

	class SashView : public wxSashWindow
	{
		typedef wxSashWindow Base;
	public:
		using Base::Base;

		wxSize GetSashClientSize();
		wxRect GetSashClientRect();

	protected:
		void OnPaint(wxPaintEvent& WXUNUSED(event));
		void OnSize(wxSizeEvent& WXUNUSED(event));

		wxDECLARE_EVENT_TABLE();
	};

	class SashLayoutView : public wxSashLayoutWindow
	{
		typedef wxSashLayoutWindow Base;
	public:
		using Base::Base;

		wxSize GetSashClientSize();
		wxRect GetSashClientRect();

	protected:
		void OnPaint(wxPaintEvent& WXUNUSED(event));
		void OnSize(wxSizeEvent& WXUNUSED(event));

		wxDECLARE_EVENT_TABLE();
	};
}
