��    l      |  �   �      0	     1	     9	     H	     U	     a	     g	     m	     q	     	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	     
     
     *
     =
     T
     l
  
   x
  4   �
     �
     �
  	   �
  
   �
  
   �
     �
     �
                 
   5     @     O  
   W     b     s  
   �     �     �     �     �     �     �     �     �                 	   (     2     >     L     Y     e     l     u     �     �     �     �     �  
   �  	   �     �  
   �     �     
  	        '  	   3     =  
   M     X  
   i     t     |  	   �     �     �     �     �     �               -  
   L     W  	   \  -   f  
   �     �     �     �     �     �     �  	   �     �       M       [     b     o  
   }     �     �     �     �     �     �     �     �     �     �     �  	   
          !     (     5     <     I     \     l          �  	   �  ,   �     �     �     �               *     7     D     ]     j     �     �     �     �     �     �     �               "     /     <     I     V     i          �     �     �     �     �     �     �     �     �     �               (     ;     K     X     h     {     �     �     �     �     �     �     �                    %     5     Q     ^     k     �     �     �     �     �  9   
     D     Q     X     e  	   �     �     �     �     �     �     �     �     �     �     8   4   h   @   S                  (           H           D   [      !       Z   /   W   9       %                   O   -   l           L       &   0         ;      B            ^   ,       +   Y   N   T   "   f      e                >           7      a   #   X   V                     G      =   \   	          _   J       
   :      3   2   ]      j          1         d   '       P       $       g   U           b               `   <   F   6   ?           i   E   M   R   k      .   Q      K          C   I          5             A           *   )       c    &Cancel &Compile	Alt+C &Copy	Ctrl+C &Delete	Del &Edit &File &OK &Paste	Ctrl+V &Quit	Ctrl+Q &Redo	Ctrl+Shift+Z &Save	Ctrl+S &Undo	Ctrl+Z About Address Available Balance BizType Cancel All Order CashIn Close All Position CloseProfit CloseRatioByMoney CloseRatioByVolume CloseTodayRatioByMoney CloseTodayRatioByVolume CommModelID Commission Couldn't find/load the '%s' catalog for locale '%s'. Credit Cu&t	Ctrl+X CurrDelta CurrMargin CurrencyID Custom anymin Custom anysec DeliveryMargin Deposit ExchangeDeliveryMargin ExchangeID ExchangeMargin FrontID FrozenCash FrozenCommission FrozenMargin FrozenSwap FundMortgageAvailable IdentifiedCardNo IdentifiedCardType InstrumentID Interest InterestBase InvestUnitID InvestorGroupID InvestorRange IsActive Limit Price LoginTime Lower Price MarginModelID Market Price MaxOrderRef Mobile Mortgage MortgageableFund Number Volume OpenDate OpenRatioByMoney OpenRatioByVolume PositionProfit PreBalance PreCredit PreDelta PreDeposit PreFundMortgageIn PreFundMortgageOut PreMargin PreMortgage Quick Buy Quick Buy Close Quick Sell Quick Sell Close RemainSwap Reserve ReserveBalance SessionID SettlementID SpecProductCloseProfit SpecProductCommission SpecProductExchangeMargin SpecProductFrozenMargin SpecProductMargin SpecProductPositionProfit SpecProductPositionProfitByAlg SystemName Tech Telephone This language is not supported by the system. TradingDay Upper Price Withdraw WithdrawQuota apply success compile error compile success help docs save success unknown Project-Id-Version: 
PO-Revision-Date: 2022-04-25 20:15+0800
Last-Translator: 
Language-Team: 
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 3.0
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: _
X-Poedit-SearchPath-0: .
 取消 编译	Alt+C 复制	Ctrl+C 删除	Del 编辑 文件 确定 粘贴	Ctrl+V 退出	Ctrl+Q 重做	Ctrl+Shift+Z 保存	Ctrl+S 撤销	Ctrl+Z 关于 通讯地址 可用资金 总资产 业务类型 全撤 资金差额 全平 平仓盈亏 平仓手续费率 平仓手续费 平今手续费率 平今手续费 手续费率模板代码 手续费 不能找到/加载%s的本地化文件%s。 信用额度 剪切	Ctrl+X 今虚实度 当前保证金总额 币种代码 自定义分 自定义秒 投资者交割保证金 入金金额 交易所交割保证金 交易所代码 交易所保证金 前置编号 冻结的资金 冻结的手续费 冻结的保证金 延时换汇冻结金额 货币质押余额 证件号码 证件类型 合约代码 利息收入 利息基数 投资单元代码 投资者分组代码 投资者范围 是否活跃 限价 登录时间 跌停 保证金率模板代码 市价 最大报单引用 手机 质押金额 可质押货币金额 数量 开户日期 开仓手续费率 开仓手续费 持仓盈亏 上次总资产 上次信用额度 昨虚实度 上次存款额 上次货币质出金额 上次货币质出金额 上次占用的保证金 上次质押金额 快买 买平 快卖 卖平 剩余换汇额度 基本准备金 保底期货结算准备金 会话编号 结算编号 特殊产品平仓盈亏 特殊产品手续费 特殊产品交易所保证金 特殊产品冻结保证金 特殊产品占用保证金 特殊产品持仓盈亏 根据持仓盈亏算法计算的特殊产品持仓盈亏 系统名称 技术 联系电话 系统不支持当前语言。 交易日 涨停 出金金额 可取资金 应用成功 编译错误 编译成功 帮助文档 保存成功 未知 