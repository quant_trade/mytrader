import zqdb.indicator as zqdb

class MyKDJ(zqdb.MinorIndicator):
    """"""

    # 这里是默认设置，其中input是用户可以通过设置界面修改的入参，result是结果数组，result数组数量代表数组列表数量
    default_setting = {
        'type':'',#趋势指标、超买超卖指标、能量指标、成交量指标、量价指标、压力支撑指标、摆动指标、反趋势指标、其他指标
        'name':'',
        'desc':'我的KDJ指标。',
        'input':{
            'N':9,
            'M1':3,
            'M2':3
        },
        'refline':[
            {'value':0, 'color':zqdb.RGB(0,255,0), 'style':zqdb.LINE_STYLE.DASHDOT},
            {'value':50, 'color':zqdb.RGB(255,255,255), 'style':zqdb.LINE_STYLE.DASHDOT},
            {'value':100, 'color':zqdb.RGB(255,0,0), 'style':zqdb.LINE_STYLE.DASHDOT}
        ],
        'result':[
            {'name':'K', 'type':zqdb.RESULT_TYPE.LINE},
            {'name':'D', 'type':zqdb.RESULT_TYPE.LINE},
            {'name':'J', 'type':zqdb.RESULT_TYPE.LINE},
            {'name':'HHV', 'type':zqdb.RESULT_TYPE.CALC},
            {'name':'LLV', 'type':zqdb.RESULT_TYPE.CALC},
            {'name':'RSV', 'type':zqdb.RESULT_TYPE.CALC}
        ]
    }

    # 构造函数，外部需要传入setting设置，用户可以在构造函数里关联计算数据/指标等
    def __init__(
        self,
        setting: dict
    ):
        """Constructor"""
        super().__init__(setting)
        input = setting["input"]
        self.N = input["N"]
        self.M1 = input["M1"]
        self.M2 = input["M2"]
        self.HIGH = zqdb.DataBuffer(zqdb.DATA.HIGH) #引用HIGH价格数组
        self.LOW = zqdb.DataBuffer(zqdb.DATA.LOW) #引用LOW价格数组
        self.CLOSE = zqdb.DataBuffer(zqdb.DATA.CLOSE) #引用CLOSE价格数组
        self.LINE_K = zqdb.ResultBuffer(0) #引用索引0的结果数组
        self.LINE_D = zqdb.ResultBuffer(1) #引用索引1的结果数组
        self.LINE_J = zqdb.ResultBuffer(2) #引用索引2的结果数组
        self.LINE_HHV = zqdb.ResultBuffer(3) #引用索引3的结果数组
        self.LINE_LLV = zqdb.ResultBuffer(4) #引用索引4的结果数组
        self.LINE_RSV = zqdb.ResultBuffer(5) #引用索引5的结果数组

    # 计算函数，当关联的计算数据/指标等更新时就会被调用
    def on_calc(self):
        """"""
        # 指标计算请在这里编写...
        # self.count DataBuffer/ResultBuffer数组大小
        # self.counted ResultBuffer数组已经计算了多少，这样可以增量计算
        
        # zqdb.KDJ(self.HIGH, self.LOW, self.CLOSE, self.N, self.M1, self.M2, self.LINE_K, self.LINE_D, self.LINE_J) 
        
        # 麦语言代码如下：
        # RSV:=(CLOSE-LLV(LOW,N))/(HHV(HIGH,N)-LLV(LOW,N))*100;
        # K:SMA(RSV,M1,1);
        # D:SMA(K,M2,1);
        # J:3*K-2*D;

        # i = self.counted
        # j = len(self.CLOSE)
        # while(i < j):
        #     self.LINE_HHV[i] = zqdb.HHV(self.HIGH,self.N,i)
        #     self.LINE_LLV[i] = zqdb.LLV(self.LOW,self.N,i)
        #     self.LINE_RSV[i] = (self.CLOSE[i]-self.LINE_LLV[i]) / (self.LINE_HHV[i]-self.LINE_LLV[i]) * 100
        #     i = i + 1

        zqdb.RSV(self.HIGH,self.LOW,self.CLOSE,self.N,self.LINE_RSV)

        zqdb.SMA(self.LINE_RSV,self.M1,1,self.LINE_K)
        zqdb.SMA(self.LINE_K,self.M2,1,self.LINE_D)
        i = self.counted
        j = len(self.CLOSE)
        while(i < j):
            self.LINE_J[i] = 3*self.LINE_K[i] - 2*self.LINE_D[i]
            i = i + 1
        pass

