import zqdb.indicator as zqdb

class MyMajor(zqdb.MajorIndicator):
    """"""

    # 这里是默认设置，其中input是用户可以通过设置界面修改的入参，result是结果数组，result数组数量代表数组列表数量
    default_setting = {
        'type':'',#趋势指标、超买超卖指标、能量指标、成交量指标、量价指标、压力支撑指标、摆动指标、反趋势指标、其他指标
        'name':'',
        'desc':'我的主图指标。',
        'input':{
            'N':5
        },
        'result':[
            {'type':zqdb.RESULT_TYPE.LINE}
        ]
    }

    # 构造函数，外部需要传入setting设置，用户可以在构造函数里关联计算数据/指标等
    def __init__(
        self,
        setting: dict
    ):
        """Constructor"""
        super().__init__(setting)
        input = setting["input"]
        self.N = input["N"]
        self.MA = zqdb.MABuffer(zqdb.DATA.CLOSE, self.N)  # 引用CLOSE价格的MA指标数组
        self.LINE = zqdb.ResultBuffer(0) #引用索引0的结果数组

    # 计算函数，当关联的计算数据/指标等更新时就会被调用
    def on_calc(self):
        """"""
        # 指标计算请在这里编写...
        # self.count CLOSE/LINE的长度
        # self.counted CLOSE/LINE已经计算了多少，这样可以增量计算
        i = self.counted
        j = len(self.MA)
        print(i, j, self.MA)
        while(i < j):
            self.LINE[i] = self.MA[i]
            i = i + 1
        pass

