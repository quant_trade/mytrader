#include <stdio.h>
#include <zqdbif.h>

int main(int argc, char* argv[])
{
	const char* settings_file = argv[argc - 1];
	printf("clear data: %s\n", settings_file);
	auto rlt = ZQDBClearData(settings_file, 1);
	printf("clear data result: %d\n", rlt);
	printf("press any key to continue...");
	getchar();
	return 0;
}
