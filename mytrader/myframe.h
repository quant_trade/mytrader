#pragma once

#include "mytechframe.h"

class MyFrame;

// -- frame --

class MyFrame : public MyTechFrame, public zqdb::MainTestMap<MyFrame>
{
	typedef MyTechFrame Base;
	typedef zqdb::MainTestMap<MyFrame> TestBase;
protected:
	wxRibbonButtonBar *sys_bar_ = nullptr;
	wxTimer test_timer_;
	zqdb::Calc::AllFunc all_strategy_func_;
	wxRibbonButtonBar *strategy_bar_ = nullptr;
	void InnerUpdateStrategy();
	void AddFirstPages();
public:
    MyFrame(const char* xml, size_t xmlflag);
    ~MyFrame();

	void OnSkinInfoChanged();
	void OnInfoChanged();

	void OnStartTest();
	void OnStopTest();
	void OnTest(uint32_t date, uint32_t time);

	void OnNotifyStatus(HZQDB h);
	void OnNotifyUpdate(HZQDB h);

protected:
	//
	
	//Navigate
	void OnNavigateSetting(wxRibbonPanelEvent& evt);
	//Market
	//Filter
	//Sort
	//TechView
	//Data
	void OnDataListen(wxRibbonButtonBarEvent& evt);
	void OnDataRecord(wxRibbonButtonBarEvent& evt);
	void UpdateTestInfo();
	void OnTestTimer(wxTimerEvent& event);
	void Test(size_t type, bool showDlg);
	void OnDataTest(wxRibbonButtonBarEvent& evt);
	void OnDataTestDropdown(wxRibbonButtonBarEvent& evt);
	void OnDataTestTest(wxCommandEvent& evt);
	void OnDataTestTradeTest(wxCommandEvent& evt);
	void OnDataTestDepthRecord(wxCommandEvent& evt);
	void OnDataTestDepthTest(wxCommandEvent& evt);
	//Calc
	void OnCalcEdit(wxRibbonButtonBarEvent& evt);
	void OnCalcEditDropdown(wxRibbonButtonBarEvent& evt);
	void OnSysGoto(wxCommandEvent& evt);
	void OnCalcTradeFlag(wxRibbonButtonBarEvent& evt);
	void OnCalcSetting(wxRibbonPanelEvent& evt);
	//Script
	//Strategy
	void UpdateStrategy(std::shared_ptr<zqdb::StrategyInfo> strategy);
	void OnStrategy(wxRibbonButtonBarEvent& evt);
	void OnStrategySetting(wxRibbonPanelEvent& evt);

	void OnCloseEvent(wxCloseEvent& evt);
    wxDECLARE_EVENT_TABLE();
};

