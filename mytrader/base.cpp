#include "base.h"

wxWindow* wxGetTopWindow(wxWindow* win)
{
	while (win) {
		if (win->IsTopLevel()) {
			return win;
		}
		win = win->GetParent();
	}
	return nullptr;
}