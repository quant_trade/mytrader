#include "mydatamodel.h"
#include "zqstr.hpp"

SmartKBManager::SmartKBManager():SmartKB()
{
	
}

void SmartKBManager::Start()
{
	SmartKB::Start();
	UpdateAll();
}

void SmartKBManager::ClearAll()
{
	SetInputItems(nullptr);
}

void SmartKBManager::UpdateAll()
{
	auto items = std::make_shared<std::vector<SmartKBItem>>();
	zqdb::AllExchange allexchange;
	for (size_t i = 0, j = ZQDBGetModuleCount(); i < j; i++)
	{
		auto hmodule = ZQDBGetModuleAt(i);
		for (auto h : allexchange)
		{
			if (ZQDBGetModule(h) != hmodule) {
				continue;
			}
			zqdb::Exchange exchange(h);
			zqdb::AllProduct allproduct(h);
			for (auto h : allproduct)
			{
				zqdb::Product product(h);
				zqdb::AllCode allcode(h);
				for (auto h : allcode)
				{
					zqdb::Code code(h);
					SmartKBItem one;
					//one.Code = zqdb::utf8_to_unicode(code->Code);
					//one.Name = zqdb::utf8_to_unicode(code->Name);
					one.Type = 0;
					one.Data = h;
					items->emplace_back(one);
				}
			}
		}
	}
	SetInputItems(items);
}

void SmartKBManager::NotifyResult(void* data)
{
	NotifyDirect((wxEvtHandler*)data);
}

void SmartKBManager::NotifyDirect(wxEvtHandler* notify)
{
	if (notify) {
		notify->QueueEvent(new wxCommandEvent(SMARTKB_SEARCH_RESULT_EVENT));
	}
}

bool SmartKBManager::Search(wxEvtHandler* notify, const wchar_t* lpszKey, int flag)
{
	return Input(notify, lpszKey, flag);
}

// ----------------------------------------------------------------------------
// SmartKBListModel
// ----------------------------------------------------------------------------

wxDEFINE_EVENT(SMARTKB_SEARCH_RESULT_EVENT, wxCommandEvent);

SmartKBListModel::SmartKBListModel() :
	wxDataViewVirtualListModel()
{
	
}

bool SmartKBListModel::Search(wxEvtHandler* notify, const wxString& strKey, int flag)
{
	return SmartKBManager::Inst().Search(notify, strKey.c_str(), flag);
}

void SmartKBListModel::ShowResult()
{
	results_.clear();
	SmartKBManager::Inst().GetInputResults(results_);
	Reset(results_.size());
}

bool SmartKBListModel::SmartKBListModel::GetResult(const wxDataViewItem& item, SmartKBItem& smkbi)
{
	auto row = GetRow(item);
	if (row >= 0 && row < results_.size()) {
		smkbi = results_[row];
		return true;
	}
	return false;
}

void SmartKBListModel::GetValueByRow(wxVariant &variant,
	unsigned int row, unsigned int col) const
{
	switch (col)
	{
	case Col_Code:
	{
		variant = results_[row].Code();
	}
	break;

	case Col_Name:
		variant = results_[row].Name();
		break;

	case Col_Desc:
	{
		static const char *labels[5] =
		{
			// These strings will look wrong without wxUSE_MARKUP, but
			// it's just a sample, so we don't care.
			"<span color=\"#87ceeb\">light</span> and "
			"<span color=\"#000080\">dark</span> blue",
			"<big>growing green</big>",
			"<i>emphatic &amp; red</i>",
			"<b>bold &amp;&amp; cyan</b>",
			"<small><tt>dull default</tt></small>",
		};

		variant = labels[row % 5];
	}
	break;
	case Col_Max:
		wxFAIL_MSG("invalid column");
	}
}

bool SmartKBListModel::GetAttrByRow(unsigned int row, unsigned int col,
	wxDataViewItemAttr &attr) const
{
	auto skin_info_ptr = wxGetApp().GetSkinInfo();
	switch (col)
	{
	case Col_Code:
		attr.SetColour(skin_info_ptr->crCtrlForgnd);
		attr.SetBackgroundColour(skin_info_ptr->crCtrlBkgnd);
		//attr.SetStrikethrough(true);
		return true;
	case Col_Name:
		attr.SetColour(skin_info_ptr->crCtrlForgnd);
		attr.SetBackgroundColour(skin_info_ptr->crCtrlBkgnd);
		return true;
		break;
	case Col_Desc:
		if (!(row % 2))
			return false;
		attr.SetColour(wxColour(*wxLIGHT_GREY));
		attr.SetStrikethrough(true);
		return true;
		break;
	case Col_Max:
		wxFAIL_MSG("invalid column");
	}
	return true;
}

bool SmartKBListModel::SetValueByRow(const wxVariant &variant,
	unsigned int row, unsigned int col)
{
	switch (col)
	{
	case Col_Code:
		return true;
	case Col_Name:
		return true;
		break;
	case Col_Desc:
		return true;
		break;
	case Col_Max:
		break;
	}

	return false;
}

// ----------------------------------------------------------------------------
// MyCodeViewListModel
// ----------------------------------------------------------------------------

SmartKBItemSort::SmartKBItemSort()
{

}

SmartKBItemSort::SmartKBItemSort(MY_CODE_SORT_TYPE type, size_t secs, int sort) :type(type), sort(sort), secs(secs)
{

}

SmartKBItemSort::SmartKBItemSort(const MDB_FIELD& field, int sort) : type(SORT_FIELD), sort(sort), field(field)
{

}

SmartKBItemSort::SmartKBItemSort(const zqdb::Calc::Sort& calc, int sort) : type(SORT_CALC_SORT), sort(sort), calc_sort(calc)
{

}

bool SmartKBItemSort::IsDynamic()
{
	switch (type)
	{
	case SORT_CALC_SORT: {
		return calc_sort.IsDynamic();
	} break;
	}
	return true;
}

bool SmartKBItemSort::operator() (const SmartKBItem& x, const SmartKBItem& y) const {
	switch (type)
	{
	case SORT_ZDF:/* {
		zqdb::Code xcode((HZQDB)x.Data), ycode((HZQDB)y.Data);
		double xzdf = 0, yzdf = 0;
		if (xcode.IsMarketValid()) {
			auto xclose = xcode->Close, xyclose = ZQDBGetYClose(xcode, true);
			xzdf = (xclose - xyclose) / xyclose;
		}
		if (ycode.IsMarketValid()) {
			auto yclose = ycode->Close, yyclose = ZQDBGetYClose(ycode, true);
			yzdf = (yclose - yyclose) / yyclose;
		}
		return xzdf < yzdf;
	} break;*/
	case SORT_ZDS: {
		return x.ValueF < y.ValueF;
	} break;
	case SORT_FIELD: {
		ASSERT(MDBFieldIsNormalized(field) && sort != 0);
		if (field.index == MDB_FIELD_INDEX(ZQDB, CODE, CODE)) {
			return x.Code() < y.Code();
		}
		else if (field.index == MDB_FIELD_INDEX(ZQDB, CODE, NAME)) {
			return x.Name() < y.Name();
		}
		else {
			zqdb::Code xcode((HZQDB)x.Data), ycode((HZQDB)y.Data);
			auto xfield = field, yfield = field;
			return xcode.GetFieldAsDouble(xfield) < ycode.GetFieldAsDouble(yfield);
		}
	} break;
	case SORT_CALC_SORT: {
		return calc_sort.Calc((HZQDB)x.Data, (HZQDB)y.Data);
	} break;
	}
	ASSERT(0);
	return false;
}

MyCodeViewListModel::MyCodeViewListModel() :
	wxDataViewVirtualListModel()
{
}

zqdb::Calc::Container MyCodeViewListModel::Cur(bool origin)
{
	if (origin) {
		if (container_) {
			return container_;
		}
	}
	zqdb::Calc::StdContainer container;
	for (auto one : results_) {
		container.AddResult((HZQDB)one.Data);
	}
	return container;
}

bool MyCodeViewListModel::IsShowAll()
{
	if(!container_)
		return key_.IsEmpty();
	return false;
}

bool MyCodeViewListModel::IsShowContainer()
{
	return container_.IsOpen();
}

bool MyCodeViewListModel::Search(wxEvtHandler* notify, const wxString& strKey, int flag)
{
	key_ = strKey;
	container_.Close();
	return SmartKBManager::Inst().Search(notify, strKey.c_str(), flag);
}

void MyCodeViewListModel::Search(wxEvtHandler* notify, const zqdb::Calc::Container& container)
{
	ASSERT(container);
	key_ = wxT("__container__");
	container_ = container;
	SmartKBManager::Inst().NotifyDirect(notify);
}

void MyCodeViewListModel::InnerUpdateOneResult(SmartKBItem& result, bool first)
{
	switch (sort_.type)
	{
	case SORT_ZDS: {
		uint32_t date = 0, time = 0;
		date = XUtil::NowDateTime(&time);
		uint32_t prev_date = date, prev_time = time;
		zqdb::Code code((HZQDB)result.Data);
		double price = code->Close;
		try {
			auto IsNotIn = code.IsNotInTradingTime(date, time);
			if (IsNotIn < 0) {
				//盘前
			}
			else {
				if (IsNotIn > 0) {
					//盘后
					uint32_t close_date = 0, close_time = 0;
					close_time = code.GetCloseTime(&close_date);
					prev_date = close_date, prev_time = close_time;
					XUtil::PrevDateTime(prev_date, prev_time, sort_.secs);
				}
				else {
					//盘中
					XUtil::PrevDateTime(prev_date, prev_time, sort_.secs);
				}
				//double yclose = ZQDBGetYClose(code, true);
				price = ZQDBGetPriceByTickTime(code, prev_date, prev_time);
			}
		}
		catch (const std::exception& ex) {
			ex;
		}
		if (!first) {
			result.OldValue = result.Value;
		}
		if (ZQDBIsInvalidValue(price) || ZQDBIsZeroValue(price)) {
			result.Value = 0;
			result.ValueF = 0;
		}
		else {
			result.Value = code->Close - price;
			result.ValueF = result.Value / price;
		}
		if (first) {
			result.OldValue = result.Value;
		}
	} break;
	default: {
		zqdb::Code code((HZQDB)result.Data);
		if (!first) {
			result.OldValue = result.Value;
		}
		if (!code.IsMarketValid(false)) {
			result.Value = 0;
			result.ValueF = 0;
		}
		else {
			auto yclose = ZQDBGetYClose(code, true);
			result.Value = code->Close - yclose;
			result.ValueF = result.Value / yclose;
		}
		if (first) {
			result.OldValue = result.Value;
		}
	} break;
	}
}

void MyCodeViewListModel::InnerUpdateResult(bool first)
{
	for (auto& result : results_)
	{
		InnerUpdateOneResult(result, first);
	}
}

void MyCodeViewListModel::UpdateResult(bool first)
{
	if (first) {
		results_.clear();
		if (IsShowContainer()) {
			ASSERT(container_);
			for (size_t i = 0, j = container_.size(); i < j; i++)
			{
				//zqdb::Code code(container_[i]);
				SmartKBItem one;
				//one.Code = zqdb::utf8_to_unicode(code->Code);
				//one.Name = zqdb::utf8_to_unicode(code->Name);
				one.Type = SMARTKB_ITEM_TYPE_CODE;
				one.Data = container_[i];
				results_.emplace_back(one);
			}
		}
		else if (IsShowAll()) {
			SmartKBManager::Inst().GetInputs(results_);
		}
		else {
			SmartKBManager::Inst().GetInputResults(results_);
		}
		InnerUpdateResult(true);
		Reset(results_.size());
	}
	else {
		InnerUpdateResult(false);
	}
}

int MyCodeViewListModel::IsSort(MY_CODE_SORT_TYPE* type, size_t* secs)
{
	if (type) {
		*type = sort_.type;
	}
	if (secs) {
		*secs = sort_.secs;
	}
	return sort_.sort;
}

void MyCodeViewListModel::Sort(bool force)
{
	if (force || sort_.IsDynamic()) {
		if (sort_.sort > 0) {
			InnerUpdateResult(false);
			std::stable_sort(results_.rbegin(), results_.rend(), sort_);
			return;
		}
		else if (sort_.sort < 0) {
			InnerUpdateResult(false);
			std::stable_sort(results_.begin(), results_.end(), sort_);
			return;
		}
		else if (force) {
			UpdateResult(true);
			return;
		}
	}
	InnerUpdateResult(false);
}

void MyCodeViewListModel::SortByZD(MY_CODE_SORT_TYPE type, size_t secs, int sort)
{
	sort_ = SmartKBItemSort(type, secs, sort);
	Sort(true);
}

void MyCodeViewListModel::SortByField(MDB_FIELD& field, int sort)
{
	sort_ = SmartKBItemSort(field, sort);
	Sort(true);
}

void MyCodeViewListModel::SortByCalc(const zqdb::Calc::Sort& calc, int sort)
{
	sort_ = SmartKBItemSort(calc, sort);
	Sort(true);
}

bool MyCodeViewListModel::GetResult(const wxDataViewItem& item, SmartKBItem& smkbi)
{
	auto row = GetRow(item);
	if (row >= 0 && row < results_.size()) {
		smkbi = results_[row];
		return true;
	}
	return false;
}

bool MyCodeViewListModel::GetResult(const size_t row, SmartKBItem& smkbi)
{
	if (row >= 0 && row < results_.size()) {
		smkbi = results_[row];
		return true;
	}
	return false;
}

int MyCodeViewListModel::FindResult(HZQDB h)
{
	if (results_.empty()) {
		return -1;
	}
	if (h) {
		switch (h->type)
		{
		case ZQDB_HANDLE_TYPE_EXCHANGE: {
			for (size_t i = 0; i < results_.size(); i++)
			{
				zqdb::Code code((HZQDB)results_[i].Data);
				if (code.GetExchange() == h) {
					return i;
					break;
				}
			}
		} break;
		case ZQDB_HANDLE_TYPE_PRODUCT: {
			for (size_t i = 0; i < results_.size(); i++)
			{
				zqdb::Code code((HZQDB)results_[i].Data);
				if (code.GetProduct() == h) {
					return i;
					break;
				}
			}
		} break;
		case ZQDB_HANDLE_TYPE_CODE: {
			for (size_t i = 0; i < results_.size(); i++)
			{
				if ((HZQDB)results_[i].Data == h) {
					return i;
					break;
				}
			}
		} break;
		default:
			break;
		}
	}
	else {
		return 0;
	}
	return -1;
}

void MyCodeViewListModel::AddResult(HZQDB h)
{
	zqdb::Code code(h);
	SmartKBItem one;
	//one.Code = zqdb::utf8_to_unicode(code->Code);
	//one.Name = zqdb::utf8_to_unicode(code->Name);
	one.Type = SMARTKB_ITEM_TYPE_CODE;
	one.Data = h;
	results_.emplace_back(one);
	Reset(results_.size());
}

int  MyCodeViewListModel::RemoveResult(const wxDataViewItem& item)
{
	auto row = GetRow(item);
	return RemoveResult(row);
}

int MyCodeViewListModel::RemoveResult(const size_t row)
{
	if (row >= results_.size()) {
		return -1;
	}
	results_.erase(results_.begin() + row);
	Reset(results_.size());
	return row;
}

void MyCodeViewListModel::GetValueByRow(wxVariant &variant,
	unsigned int row, unsigned int col) const
{
	switch (col)
	{
	case Col_Code:
	{
		variant = (void*)&results_[row];
	}
	break;
	case Col_Max:
		wxFAIL_MSG("invalid column");
	}
}

bool MyCodeViewListModel::GetAttrByRow(unsigned int row, unsigned int col,
	wxDataViewItemAttr &attr) const
{
	switch (col)
	{
	case Col_Code:
		//attr.SetColour(skin_info_ptr->crCtrlForgnd);
		//attr.SetBackgroundColour(skin_info_ptr->crCtrlBkgnd);
		//attr.SetStrikethrough(true);
		return true;
	case Col_Max:
		wxFAIL_MSG("invalid column");
	}
	return true;
}

bool MyCodeViewListModel::SetValueByRow(const wxVariant &variant,
	unsigned int row, unsigned int col)
{
	switch (col)
	{
	case Col_Code:
		return true;
		break;
	case Col_Max:
		break;
	}

	return false;
}

// ----------------------------------------------------------------------------
// MyCodeListModel
// ----------------------------------------------------------------------------

MyCodeListModel::MyCodeListModel() :
	wxDataViewVirtualListModel()
{
	
}

void MyCodeListModel::Show(const std::vector<HZQDB>& h_list)
{
	code_items_.resize(h_list.size());
	for (size_t i = 0, j = code_items_.size(); i < j; ++i)
	{
		code_items_[i] = h_list[i];
	}
	Reset(code_items_.size());
}

HZQDB MyCodeListModel::GetData(const wxDataViewItem& item)
{
	auto row = GetRow(item);
	if (row >= 0 && row < code_items_.size()) {
		return code_items_[row];
	}
	return nullptr;
}

void MyCodeListModel::GetValueByRow(wxVariant &variant,
	unsigned int row, unsigned int col) const
{
	switch (col)
	{
	case Col_Code:
	{
		variant = code_items_[row];
	}
	break;

	case Col_Name:
		variant = code_items_[row];
		break;

	case Col_Desc:
	{
		static const char *labels[5] =
		{
			// These strings will look wrong without wxUSE_MARKUP, but
			// it's just a sample, so we don't care.
			"<span color=\"#87ceeb\">light</span> and "
			"<span color=\"#000080\">dark</span> blue",
			"<big>growing green</big>",
			"<i>emphatic &amp; red</i>",
			"<b>bold &amp;&amp; cyan</b>",
			"<small><tt>dull default</tt></small>",
		};

		variant = labels[row % 5];
	}
	break;
	case Col_Max:
		wxFAIL_MSG("invalid column");
	}
}

bool MyCodeListModel::GetAttrByRow(unsigned int row, unsigned int col,
	wxDataViewItemAttr &attr) const
{
	auto skin_info_ptr = wxGetApp().GetSkinInfo();
	switch (col)
	{
	case Col_Code:
		attr.SetColour(skin_info_ptr->crCtrlForgnd);
		attr.SetBackgroundColour(skin_info_ptr->crCtrlBkgnd);
		return true;
	case Col_Name:
		attr.SetColour(skin_info_ptr->crCtrlForgnd);
		attr.SetBackgroundColour(skin_info_ptr->crCtrlBkgnd);
		return true;
		break;
	case Col_Desc:
		if (!(row % 2))
			return false;
		attr.SetColour(wxColour(*wxLIGHT_GREY));
		attr.SetStrikethrough(true);
		return true;
		break;
	case Col_Max:
		wxFAIL_MSG("invalid column");
	}
	return true;
}

bool MyCodeListModel::SetValueByRow(const wxVariant &variant,
	unsigned int row, unsigned int col)
{
	switch (col)
	{
	case Col_Code:
		return true;
	case Col_Name:
		return true;
		break;
	case Col_Desc:
		return true;
		break;
	case Col_Max:
		break;
	}

	return false;
}

//// ----------------------------------------------------------------------------
//// MyHZQDBListModel
//// ----------------------------------------------------------------------------
//
//MyHZQDBListModel::MyHZQDBListModel(const char* xml, size_t xmlflag) : Base()
//{
//	CFG_FROM_XML(cfg, xml, xmlflag);
//	auto opt_col = cfg.get_child_optional("col");
//	ASSERT(opt_col);
//	auto& cfg_col = opt_col.get();
//	BOOST_FOREACH(const boost::property_tree::ptree::value_type &module_pr, cfg_col)
//	{
//		auto& col_items = all_col_items_[module_pr.first];
//		BOOST_FOREACH(const boost::property_tree::ptree::value_type &col_pr, module_pr.second)
//		{
//			auto& col_one = col_pr.second;
//			ColInfo col_item;
//			strncpy(col_item.field.name, col_one.get<std::string>("id", "").c_str(), MAX_FIELD_NAME);
//			col_item.name = utf2wxString(col_one.get<std::string>("name", "").c_str());
//			col_item.precision = col_one.get<int>("precision", 0);
//			col_items.emplace_back(col_item);
//		}
//	}
//}
//
//void MyHZQDBListModel::Select(HZQDB user, ZQDB_HANDLE_TYPE type)
//{
//	zqdb::ObjectT<tagModuleInfo> module(ZQDBGetParent(user));
//	col_items_ = all_col_items_[module->Name];
//	for (auto& item : col_items_)
//	{
//		module.NormalizeField(item.field, type);
//	}
//}

// ----------------------------------------------------------------------------
// MyCatModelNode: a node inside MyCatModel
// ----------------------------------------------------------------------------

MyCatModelNode::MyCatModelNode(const wxString &title, char* type, int count) :title_(title), parent_(nullptr)
{
	zqdb::AllProduct all;
	for (size_t i = 0, j = all.size(); i < j; i++)
	{
		zqdb::Product product(all[i]);
		if (type) {
			for (size_t k = 0; k < count; k++)
			{
				if (type[k] == product->Type) {
					childs_.Add(new MyCatModelNode(this, all[i]));
					break;
				}
			}
		}
		else {
			childs_.Add(new MyCatModelNode(this, all[i], type, count));
		}
	}
}

MyCatModelNode::MyCatModelNode(MyCatModelNode* parent, HZQDB h, char* type, int count) :h_(h), parent_(parent)
{
	wxASSERT(h);
	if (h->type == ZQDB_HANDLE_TYPE_EXCHANGE) {
		zqdb::Exchange exchange(h);
		title_ = utf2wxString(exchange->Name);
		zqdb::AllProduct all(h);
		for (size_t i = 0, j = all.size(); i < j; i++)
		{
			if (type) {
				zqdb::Product product(all[i]);
				for (size_t j = 0; j < count; j++)
				{
					if (type[j] == product->Type) {
						childs_.Add(new MyCatModelNode(this, all[i]));
						break;
					}
				}
			}
			else {
				childs_.Add(new MyCatModelNode(this, all[i]));
			}
		}
	}
	else {
		wxASSERT(h->type == ZQDB_HANDLE_TYPE_PRODUCT);
		zqdb::Product product(h);
		title_ = utf2wxString(product->Name);
		state_ = 1;
	}
}

MyCatModelNode::~MyCatModelNode()
{
	// free all our children nodes
	size_t count = childs_.GetCount();
	for (size_t i = 0; i < count; i++)
	{
		MyCatModelNode *child = childs_[i];
		delete child;
	}
}

// ----------------------------------------------------------------------------
// MyCatModel
// ----------------------------------------------------------------------------

MyCatModel::MyCatModel()
{
	//m_root = new MyCatModelNode(wxT("全部"), type, count);
}

MyCatModel::~MyCatModel()
{
	// free all our children nodes
	size_t count = roots_.GetCount();
	for (size_t i = 0; i < count; i++)
	{
		MyCatModelNode *child = roots_[i];
		delete child;
	}
}

wxDataViewItem MyCatModel::AddRoot(const wxString& title, char* type, int count)
{
	// new
	auto node = new MyCatModelNode(title, type, count);
	// add
	roots_.Add(node);
	// notify control
	wxDataViewItem item((void*)node);
	ItemAdded(wxDataViewItem((void*)nullptr), item);
	return item;
}

void MyCatModel::RemoveRoot(const wxString& title)
{
	for (auto it = roots_.begin(); it != roots_.end(); ++it) {
		auto node = *it;
		if (node->title_ == title) {
			// erase
			roots_.erase(it);
			// notify control
			ItemDeleted(wxDataViewItem(node->GetParent()), wxDataViewItem(node));
			// delete
			delete node;
			break;
		}
	}
}

wxDataViewItemArray MyCatModel::GetRoots() const
{
	wxDataViewItemArray arr;
	for (auto root : roots_) {
		arr.Add(wxDataViewItem((void*)root));
	}
	return arr;
}

bool MyCatModel::EnableState(bool enable)
{
	enable_state_ = enable;
	//ItemsChanged(GetRoots());
	return true;
}

void MyCatModel::UpdateProgress(const wxDataViewItem &item, int value)
{
	MyCatModelNode *node = (MyCatModelNode*)item.GetID();
	if (!node)
		return;

	node->progress_ = value; 
	ValueChanged(item, 2);
}

void MyCatModel::UpdateInfo(const wxDataViewItem &item, const wxString& value)
{
	MyCatModelNode *node = (MyCatModelNode*)item.GetID();
	if (!node)
		return;

	node->info_ = value;
	ValueChanged(item, 3);
}

HZQDB MyCatModel::GetHandle(const wxDataViewItem &item) const
{
	MyCatModelNode *node = (MyCatModelNode*)item.GetID();
	if (!node)
		return nullptr;

	return node->h_;
}

wxString MyCatModel::GetTitle(const wxDataViewItem &item) const
{
	MyCatModelNode *node = (MyCatModelNode*)item.GetID();
	if (!node)
		return wxEmptyString;

	return node->title_;
}

wxString MyCatModel::GetInfo(const wxDataViewItem &item) const
{
	MyCatModelNode *node = (MyCatModelNode*)item.GetID();
	if (!node)
		return wxEmptyString;

	return node->info_;
}

int MyCatModel::GetState(const wxDataViewItem &item) const
{
	MyCatModelNode *node = (MyCatModelNode*)item.GetID();
	if (!node)
		return 0;

	return node->state_;
}

int MyCatModel::GetProgress(const wxDataViewItem &item) const
{
	MyCatModelNode *node = (MyCatModelNode*)item.GetID();
	if (!node)
		return 0;

	return node->progress_;
}

int MyCatModel::Compare(const wxDataViewItem &item1, const wxDataViewItem &item2,
	unsigned int column, bool ascending) const
{
	wxASSERT(item1.IsOk() && item2.IsOk());
	// should never happen

	if (IsContainer(item1) && IsContainer(item2))
	{
		wxVariant value1, value2;
		GetValue(value1, item1, 0);
		GetValue(value2, item2, 0);

		wxString str1 = value1.GetString();
		wxString str2 = value2.GetString();
		int res = str1.Cmp(str2);
		if (res) return res;

		// items must be different
		wxUIntPtr litem1 = (wxUIntPtr)item1.GetID();
		wxUIntPtr litem2 = (wxUIntPtr)item2.GetID();

		return litem1 - litem2;
	}

	return wxDataViewModel::Compare(item1, item2, column, ascending);
}

void MyCatModel::GetValue(wxVariant &variant,
	const wxDataViewItem &item, unsigned int col) const
{
	wxASSERT(item.IsOk());

	MyCatModelNode *node = (MyCatModelNode*)item.GetID();
	switch (col)
	{
	case 0:
		variant = node->title_;
		break;
	case 1:
		variant = (bool)node->state_;
		break;
	case 2:
		variant = (long)node->progress_;
		break;
	case 3:
		variant = node->info_;
		break;

	default:
		wxLogError("MyCatModel::GetValue: wrong column %d", col);
	}
}

bool MyCatModel::SetValue(const wxVariant &variant,
	const wxDataViewItem &item, unsigned int col)
{
	wxASSERT(item.IsOk());

	MyCatModelNode *node = (MyCatModelNode*)item.GetID();
	switch (col)
	{
	case 0:
		node->title_ = variant.GetString();
		return true;
	case 1:
		node->state_ = variant.GetBool();
		return true;
	case 2:
		node->progress_ = variant.GetInteger();
		return true;
	case 3:
		node->info_ = variant.GetString();
		return true;

	default:
		wxLogError("MyCatModel::SetValue: wrong column");
	}
	return false;
}

bool MyCatModel::IsEnabled(const wxDataViewItem &item,
	unsigned int col) const
{
	//wxASSERT(item.IsOk());

	//MyCatModelNode *node = (MyCatModelNode*)item.GetID();
	if(col == 1) { 
		return enable_state_;
	}

	// disable Beethoven's ratings, his pieces can only be good
	return true;
}

wxDataViewItem MyCatModel::GetParent(const wxDataViewItem &item) const
{
	// the invisible root node has no parent
	if (!item.IsOk())
		return wxDataViewItem(0);

	MyCatModelNode *node = (MyCatModelNode*)item.GetID();

	// "MyMusic" also has no parent
	//if (node == m_root)
	//	return wxDataViewItem(0);

	return wxDataViewItem((void*)node->GetParent());
}

bool MyCatModel::IsContainer(const wxDataViewItem &item) const
{
	// the invisble root node can have children
	// (in our model always "MyMusic")
	if (!item.IsOk())
		return true;

	MyCatModelNode *node = (MyCatModelNode*)item.GetID();
	return node->IsContainer();
}

unsigned int MyCatModel::GetChildren(const wxDataViewItem &parent,
	wxDataViewItemArray &array) const
{
	MyCatModelNode *node = (MyCatModelNode*)parent.GetID();
	if (!node)
	{
		auto roots = GetRoots();
		for (auto root : roots) {
			array.Add(root);
		}
		return roots.size();
	}

	if (node->GetChildCount() == 0)
	{
		return 0;
	}

	unsigned int count = node->GetChildren().GetCount();
	for (unsigned int pos = 0; pos < count; pos++)
	{
		MyCatModelNode *child = node->GetChildren().Item(pos);
		array.Add(wxDataViewItem((void*)child));
	}

	return count;
}

