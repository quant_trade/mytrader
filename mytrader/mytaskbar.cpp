#include "mytaskbar.h"
#include "myapp.h"
#include "myframe.h"

// ----------------------------------------------------------------------------
// MyTaskBarIcon implementation
// ----------------------------------------------------------------------------

enum
{
    PU_INFO = 10001,
	PU_ABOUT,
	PU_CLOSE_SOUND,
	PU_RESTORE,
	PU_CLOSEDATA,
	PU_DOWNLOADDATA,
	PU_EXPORTDATA,
	PU_CLEARDATA,
	PU_RESETTINGS,
	PU_REPORTCONTEXT,
	PU_RESTART,
	PU_CLOSE_EXIT,
    PU_EXIT,
};


wxBEGIN_EVENT_TABLE(MyTaskBarIcon, wxTaskBarIcon)
    EVT_MENU(PU_RESTORE, MyTaskBarIcon::OnMenuRestore)
	EVT_MENU(PU_ABOUT, MyTaskBarIcon::OnAbout)
	EVT_MENU(PU_CLOSE_SOUND, MyTaskBarIcon::OnCloseSound)
	EVT_MENU(PU_CLOSEDATA, MyTaskBarIcon::OnCloseData)
	EVT_MENU(PU_DOWNLOADDATA, MyTaskBarIcon::OnDownloadData)
	EVT_MENU(PU_EXPORTDATA, MyTaskBarIcon::OnExportData)
	EVT_MENU(PU_CLEARDATA, MyTaskBarIcon::OnClearData)
	EVT_MENU(PU_RESETTINGS, MyTaskBarIcon::OnResettings)
	EVT_MENU(PU_REPORTCONTEXT, MyTaskBarIcon::OnReportContext)
	EVT_MENU(PU_RESTART, MyTaskBarIcon::OnRestart)
	EVT_MENU(PU_CLOSE_EXIT, MyTaskBarIcon::OnMenuCloseExit)
	EVT_MENU(PU_EXIT, MyTaskBarIcon::OnMenuExit)
	EVT_TASKBAR_LEFT_DCLICK(MyTaskBarIcon::OnLeftButtonDClick)
wxEND_EVENT_TABLE()

void MyTaskBarIcon::OnMenuRestore(wxCommandEvent& )
{
	wxGetApp().ShowFrame();
}

void MyTaskBarIcon::OnAbout(wxCommandEvent&)
{
	wxGetApp().ShowAbout();
}

void MyTaskBarIcon::OnCloseSound(wxCommandEvent& evt)
{
	wxGetApp().SetCloseSound(evt.IsChecked());
}

void MyTaskBarIcon::OnCloseData(wxCommandEvent&)
{
	wxGetApp().SaveClose();
}

void MyTaskBarIcon::OnDownloadData(wxCommandEvent&)
{
	wxGetApp().DownloadData();
}

void MyTaskBarIcon::OnExportData(wxCommandEvent&)
{
	wxGetApp().ExportData();
}

void MyTaskBarIcon::OnClearData(wxCommandEvent&)
{
	wxGetApp().ClearData();
}

void MyTaskBarIcon::OnResettings(wxCommandEvent&)
{
	wxGetApp().Resettings(wxGetApp().IsRunFlag());
}

void MyTaskBarIcon::OnReportContext(wxCommandEvent&)
{
	// example of manually generated report, this could be also
	// used in wxApp::OnAssert()
	wxGetApp().GenerateReport(wxDebugReport::Context_Current);
}

void MyTaskBarIcon::OnRestart(wxCommandEvent&)
{
	wxGetApp().Restart(wxGetApp().IsRunFlag());
}

void MyTaskBarIcon::OnMenuCloseExit(wxCommandEvent&)
{
	if (wxGetApp().AnyModalDlg()) {
		wxMessageBox(wxT("主界面有对话框未关闭!"), wxT("提示"), wxOK);
		return;
	}
	wxGetApp().PostExit(EXIT_FLAG_CLOSE);
}

void MyTaskBarIcon::OnMenuExit(wxCommandEvent& )
{
	if (wxGetApp().AnyModalDlg()) {
		wxMessageBox(wxT("主界面有对话框未关闭!"), wxT("提示"),wxOK);
		return;
	}
    wxGetApp().PostExit();
}

// Overridables
wxMenu *MyTaskBarIcon::CreatePopupMenu()
{
	wxMenu *menu = new wxMenu;
	if (wxGetApp().IsInitFlag()) {
		//wxString strAppStatus = wxGetApp().IsRunFlag() ? wxGetApp().GetAppStatus(true) : wxGetApp().GetAppTitle();
		//menu->Append(PU_INFO, strAppStatus);
		//menu->Enable(PU_INFO, false);
		//menu->AppendSeparator();
		menu->Append(PU_ABOUT, _("About"));
		auto item = menu->Append(PU_CLOSE_SOUND, wxT("关闭声音"), wxEmptyString, wxITEM_CHECK);
		menu->Check(item->GetId(), wxGetApp().GetCloseSound());
		if (wxGetApp().IsRunFlag()) {
			menu->AppendSeparator();
			menu->Append(PU_RESTORE, wxT("打开主窗口"));
		}
//		if (!ZQDBIsTest()) {
//			menu->AppendSeparator(); 
//			//menu->Append(PU_PREFERENCE, "设置");
//			if (ZQDBIsRPC()) {
//				menu->Append(PU_DOWNLOADDATA, "盘后数据下载");
//			}
//#ifndef _DEBUG
//			else 
//#endif
//			{
//				menu->Append(PU_CLOSEDATA, "强制收盘");
//			}
//			menu->Append(PU_EXPORTDATA, "保存数据");
//			menu->Append(PU_CLEARDATA, "清理数据");
//			if (!g_full) {
//				menu->Append(PU_RESETTINGS, "重新设置");
//			}
//			//menu->Append(PU_REPORTCONTEXT, "报告上下文");
//			menu->AppendSeparator();
//			menu->Append(PU_RESTART, wxT("重启"));
//		}
	}
	else {
		menu->Append(PU_ABOUT, _("About"));
		/*menu->AppendSeparator();
		menu->Append(PU_RESETTINGS, "重新设置");*/
	}
	/*menu->AppendSeparator();
	menu->Append(PU_NEW_ICON, "&Set New Icon");
	menu->AppendSeparator();
	wxMenu *submenu = new wxMenu;
	submenu->Append(PU_SUB1, "监控");
	submenu->Append(PU_SUB2, "策略");
	menu->Append(PU_SUBMAIN, "新建", submenu);*/
	//menu->AppendSeparator();
	//menu->AppendCheckItem(PU_CHECKMARK, "Test &check mark");
    /* OSX has built-in quit menu for the dock menu, but not for the status item */
#ifdef __WXOSX__
    if ( OSXIsStatusItem() )
#endif
    {
		menu->AppendSeparator();
		if (wxGetApp().IsInitFlag()) {
			if (wxGetApp().IsRunFlag()) {
				if (!ZQDBIsRPC()) {
					menu->Append(PU_CLOSE_EXIT, wxT("收盘退出"));
				}
			}
		}
        menu->Append(PU_EXIT, wxT("退出"));
    }
    return menu;
}

void MyTaskBarIcon::OnLeftButtonDClick(wxTaskBarIconEvent&)
{
	if (wxGetApp().IsRunFlag()) {
		wxGetApp().ShowFrame();
	}
	else {
		//wxMessageBox(wxT("正在初始化..."), wxT("提示"), wxOK);
	}
}
