#pragma once

#include "wx/wxprec.h"

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#include "wx/app.h"
#include "wx/frame.h"
#include "wx/textctrl.h"
#include "wx/ribbon/bar.h"
#include "wx/ribbon/buttonbar.h"
#include "wx/ribbon/gallery.h"
#include "wx/ribbon/toolbar.h"
#include "wx/sizer.h"
#include "wx/menu.h"
#include "wx/msgdlg.h"
#include "wx/dcbuffer.h"
#include "wx/colordlg.h"
#include "wx/artprov.h"
#include "wx/combobox.h"
#include "wx/tglbtn.h"
#include "wx/wrapsizer.h"

#include <view.h>
#include <XUtil/XThread.hpp>
#include "settings.h"
#include "mydatamodel.h"
#include "mydataview.h"
#include "mymodulemgr.h"

class MyBaseFrame;

class MyTitleBar : public wxPanel
	, public zqdb::SkinMap<MyTitleBar, SkinInfo>
{
	typedef wxPanel Base;
	typedef zqdb::SkinMap<MyTitleBar, SkinInfo> SkinBase;
public:
	MyTitleBar(wxWindow* parent);
	~MyTitleBar();

	void OnSkinInfoChanged();

protected:
	void OnErase(wxEraseEvent& e) {};
	void OnPaint(wxPaintEvent& e);
	void Paint(wxDC& dc);
	void OnSize(wxSizeEvent& e);
	void OnEnterWindow(wxMouseEvent& event);
	void OnLeaveWindow(wxMouseEvent& event);
protected:
	void OnMotion(wxMouseEvent& event);
	void OnLeftDown(wxMouseEvent& event);
	void OnLeftUp(wxMouseEvent& event);
	void OnMouseLost(wxMouseCaptureLostEvent& event);
private:
	bool m_bInside;
	wxPoint m_offset;
	//wxBitmap m_min_bmps[2];
	wxBitmap m_close_bmps[2];
	//wxRect m_min_rect;
	wxRect m_close_rect;
	int m_close_status = 0;
private:
	DECLARE_EVENT_TABLE()
};

class MyRibbonMSWArtProvider : public wxRibbonMSWArtProvider
{
	typedef wxRibbonMSWArtProvider Base;
protected:
	SkinStyle style_;
public:
	MyRibbonMSWArtProvider(SkinStyle style = SKIN_NONE);

	wxRibbonArtProvider* Clone() const wxOVERRIDE;
	void CloneTo(MyRibbonMSWArtProvider* copy) const;

	void SetColourScheme(const wxColour& primary,
		const wxColour& secondary,
		const wxColour& tertiary);

	virtual void DrawTool(
		wxDC& dc,
		wxWindow* wnd,
		const wxRect& rect,
		const wxBitmap& bitmap,
		wxRibbonButtonKind kind,
		long state);
};

class MyRibbonBar : public wxRibbonBar
{
	typedef wxRibbonBar Base;
private:
	wxIcon icon_;
	wxPoint offset_;
	wxRect close_button_rect_;
	wxRect max_button_rect_;
	wxRect min_button_rect_;
	bool close_button_hovered_ = false;
	bool max_button_hovered_ = false;
	bool min_button_hovered_ = false;
public:
	MyRibbonBar(wxWindow* parent,
		wxWindowID id = wxID_ANY,
		const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxDefaultSize,
		long style = wxRIBBON_BAR_DEFAULT_STYLE);

protected:
	//
	void OnPaint(wxPaintEvent& evt);
	void OnSize(wxSizeEvent& evt);
	void OnMouseMove(wxMouseEvent& evt);
	void OnMouseLeave(wxMouseEvent& evt);
	void OnMouseLost(wxMouseCaptureLostEvent& evt);
	void OnMouseLeftDown(wxMouseEvent& evt);
	void OnMouseLeftUp(wxMouseEvent& evt);
	void OnMouseMiddleDown(wxMouseEvent& evt);
	void OnMouseMiddleUp(wxMouseEvent& evt);
	void OnMouseRightDown(wxMouseEvent& evt);
	void OnMouseRightUp(wxMouseEvent& evt);
	void OnMouseDoubleClick(wxMouseEvent& evt);
	
	wxDECLARE_EVENT_TABLE();
};

class MyStatusBarBase: public wxStatusBar
{
public:
	typedef MyStatusBarBase This;
	typedef wxStatusBar Base;
protected:
	wxPoint m_offset;
public:
	using Base::Base;

protected:
	// event handlers
	void OnSetCursor(wxSetCursorEvent& event);
	void OnMotion(wxMouseEvent& event);
	void OnLeftDown(wxMouseEvent& event);
	void OnLeftUp(wxMouseEvent& event);
	void OnMouseLost(wxMouseCaptureLostEvent& event);

	wxDECLARE_EVENT_TABLE();
};

// -- frame --

class MyBaseFrame : public wxFrame
	, public zqdb::SkinMap<MyBaseFrame, SkinInfo>
{
	typedef wxFrame Base;
	typedef zqdb::SkinMap<MyBaseFrame, SkinInfo> SkinBase;
public:
	WNDPROC old_wndproc_ = nullptr;
	static LRESULT CALLBACK MyWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
protected:
	//RibbonBar ID����
	enum
	{
		ID_PAGE_BEGIN = wxID_HIGHEST + 1,
		ID_PAGE_START,
		ID_PAGE_CALC,
		ID_PAGE_NEW,
		ID_PAGE_NEW_PAGE,
		ID_PAGE_NEW_PAGE_MAX = ID_PAGE_NEW_PAGE + 16,
		ID_PAGE_HELP,
		ID_PAGE_END
	};
	wxRibbonBar* m_ribbon;
	std::map<MyBaseFrame*, wxRibbonPage*> frames_;
	//size_t AddExchangePage(HZQDB h, size_t product_id);
	std::map<wxRibbonPage*, std::function<void(wxRibbonBarEvent&)>> last_pages_;
	virtual void AddFirstPages() { }
	virtual void AddLastPages();
	void RemoveLastPages();
	//wxTextCtrl* m_logwindow;
	//wxToggleButton* m_togglePanels;
#if wxUSE_INFOBAR
	wxInfoBar* infobar_ = nullptr;
#endif//
public:
    MyBaseFrame(const char* xml, size_t xmlflag);
    ~MyBaseFrame();

	void SetTitle(const wxString& title);

	void AddFrame(MyBaseFrame* frame);
	void RemoveFrame(MyBaseFrame* frame);
	void ActivateFrame(MyBaseFrame* frame);
	void UpdateAllPages();

	// show the info bar with the given message and optionally an icon
	void ShowMessage(const wxString& msg,
		int flags = wxICON_INFORMATION);
	void HideMessage();

	virtual int FilterEvent(wxEvent& event);

	virtual void OnSkinInfoChanged();

protected:
	//
	void OnPageChanging(wxRibbonBarEvent& evt); 
	void OnHelpClicked(wxRibbonBarEvent& evt);
	void OnErase(wxEraseEvent &evt);
	void OnPaint(wxPaintEvent& evt);
	void OnMaximizeEvent(wxMaximizeEvent& evt);
    void OnSizeEvent(wxSizeEvent& evt);
	void OnCloseEvent(wxCloseEvent& evt);

	void OnTimer(wxTimerEvent& evt);
    wxDECLARE_EVENT_TABLE();
};

