#include "mylogindlg.h"


// ----------------------------------------------------------------------------
// MyLoginDlg implementation
// ----------------------------------------------------------------------------

wxBEGIN_EVENT_TABLE(MyLoginDlg, Base)
	EVT_BUTTON(wxID_OK, MyLoginDlg::OnOK)
	EVT_BUTTON(wxID_IGNORE, MyLoginDlg::OnIgnore)
	EVT_BUTTON(wxID_CANCEL, MyLoginDlg::OnCancel)
wxEND_EVENT_TABLE()

MyLoginDlg* MyLoginDlg::s_dlg = nullptr;

MyLoginDlg::MyLoginDlg(wxWindow *parent)
	:Base(parent, wxID_ANY, wxT("�����˺�"), wxDefaultPosition, wxDefaultSize
		, wxDEFAULT_DIALOG_STYLE
		| wxCLIP_CHILDREN
		//| wxCLIP_SIBLINGS
	)
{
	SetIcon(wxICON(mytrader));
	wxASSERT(!s_dlg);
	s_dlg = this; //

	stc_bmp_ = new wxStaticBitmap(this, wxID_ANY, wxGetApp().GetSkinInfo()->bmpWizard,
			wxDefaultPosition, wxDefaultSize,
			0);

	stc_type_ = new wxStaticText(this, wxID_ANY, wxT("��ѡ���˺����"), wxDefaultPosition, wxDefaultSize, wxALIGN_BOTTOM);
	cmb_type_ = new wxComboBox(this, wxID_ANY,
		wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, nullptr, wxCB_READONLY);
	wxGetApp().BroadcastModule([this](std::shared_ptr<zqdb::Module> module) -> bool {
		if ((*module)->flags | ZQDB_MODULE_FLAG_TRADE) {
			auto Code = (*module)->Code;
			auto Name = (*module)->Name;
			if (ZQDBIsTest()) {
				if (stricmp(Code, "ctp") == 0) {
					cmb_type_->Append(wxT("�ڻ������˺�"), (void*)module.get());
				}
				else if (stricmp(Code, "tora") == 0) {
					cmb_type_->Append(wxT("��Ʊ�����˺�"), (void*)module.get());
				}
				else {
					cmb_type_->Append(utf2wxString(Name) + wxT("�����˺�"), (void*)module.get());
				}
			}
			else {
				cmb_type_->Append(utf2wxString(Name) + wxT("�˺�"), module.get());
				if (stricmp(Code, "ctp") == 0) {
					cmb_type_->Append(wxT("�ڻ�ģ���˺�"), (void*)nullptr);
				}
				else if (stricmp(Code, "tora") == 0) {
					cmb_type_->Append(wxT("��Ʊģ���˺�"), (void*)nullptr);
				}
			}
		}
		return false;
	});

	stc_view_ = new wxStaticText(this, wxID_ANY, wxT("�����������˺���Ϣ"), wxDefaultPosition, wxDefaultSize, wxALIGN_BOTTOM);

	btn_ok_ = new wxButton(this, wxID_OK, wxT("��¼"), wxDefaultPosition, wxSize(100, 35));
	//btn_ignore_ = new wxButton(this, wxID_IGNORE, "&Ignore");
	btn_cancel_ = new wxButton(this, wxID_CANCEL, wxT("ȡ��"), wxDefaultPosition, wxSize(100, 35));

	stc_tips_ = new wxStaticText(this, wxID_ANY, wxT("��ѡ���˺�������������˺���Ϣ��Ȼ������¼"), wxDefaultPosition, wxDefaultSize, wxALIGN_BOTTOM);

	cmb_type_->SetSelection(0);
	UpdateType();

	wxBoxSizer *sizerAll = new wxBoxSizer(wxHORIZONTAL);

	wxBoxSizer *sizerLeft = new wxBoxSizer(wxVERTICAL);

	sizerLeft->Add(
		stc_bmp_,
		1, // No stretching
		wxALL,
		5
	);

	sizerAll->Add(sizerLeft, 0, wxEXPAND);

	wxBoxSizer *sizerTop = new wxBoxSizer(wxVERTICAL);

	sizerTop->Add(
		stc_type_,
		0, // No stretching
		wxEXPAND | wxUP | wxRIGHT | wxLEFT, 5
	);
	sizerTop->Add(
		cmb_type_,
		0, // No stretching
		wxEXPAND | wxDOWN | wxRIGHT | wxLEFT, 5
	);

	sizerTop->Add(
		stc_view_,
		0, // No stretching
		wxEXPAND | wxRIGHT | wxLEFT, 5
	);
	sizerTop->Add(
		view_,
		1, // stretching
		wxEXPAND | wxRIGHT | wxLEFT, 5
	);

	wxBoxSizer *sizerBottom = new wxBoxSizer(wxHORIZONTAL);
	sizerBottom->AddStretchSpacer();
	sizerBottom->Add(btn_ok_, 0, wxALIGN_CENTER | wxALL, 5);
	//sizerBottom->Add(btn_ignore_, 0, wxALIGN_CENTER | wxALL, 5);
	sizerBottom->Add(btn_cancel_, 0, wxALIGN_CENTER | wxALL, 5);
	sizerBottom->AddStretchSpacer();
	sizerTop->Add(sizerBottom, 0, wxEXPAND | wxALL, 5);

	sizerTop->Add(stc_tips_, 0, wxEXPAND | wxRIGHT | wxLEFT, 5);

	sizerAll->Add(sizerTop, 1, wxEXPAND);

	SetSizerAndFit(sizerAll);

	Centre();

	SetEscapeId(wxID_CANCEL);

	btn_ok_->SetFocus();
	btn_ok_->SetDefault();
	cmb_type_->Bind(wxEVT_COMBOBOX, &MyLoginDlg::OnCmbTypeUpdate, this);
}

MyLoginDlg::~MyLoginDlg()
{
	s_dlg = nullptr;
}

void MyLoginDlg::OnSkinInfoChanged()
{
	//SetBackgroundColour(skin_info_ptr_->crPrimary);
}
//
//void MyLoginDlg::OnNotifyAdd(HZQDB h)
//{
//	DealUserLogin(h);
//}
//
//void MyLoginDlg::OnNotifyUpdate(HZQDB h)
//{
//	DealUserLogin(h);
//}
//
//void MyLoginDlg::DealUserLogin(HZQDB h)
//{
//	if (h->type != ZQDB_HANDLE_TYPE_USER)
//		return;
//	if (user_) {
//		zqdb::ObjectT<tagUserInfo> user(h);
//		if (user->Status == USER_STATUS_LOGGED) {
//			if (strstr(user_, user->User)) {
//				EndModal(wxID_OK);
//			}
//		}
//	}
//}

void MyLoginDlg::UpdateType()
{
	auto sel = cmb_type_->GetSelection();
	auto module = cmb_type_->GetClientData(sel);
	char* broker = nullptr;
	if (ZQDBIsTest()) {
		broker = ZQDB_USER_BROKER_TEST;
	}
	else {
		if (!module) {
			broker = ZQDB_USER_BROKER_MOCK;
			module = cmb_type_->GetClientData(sel - 1);
		}
	}
	auto& allmodule = wxGetApp().AllModule();
	for (size_t i = 0, j = allmodule.size(); i < j; i++)
	{
		if (allmodule[i].get() == module) {
			module_ = std::dynamic_pointer_cast<MyModule>(allmodule[i]);
			if (!broker) {
				broker = (*module_)->Code;
			}
		}
	}
	char buf[1024] = { 0 };
	sprintf(buf, R"({"%s":"%s"})", STR_MDB_FIELD_INDEX(ZQDB, USER, BROKER), broker);
	auto view = module_->NewLoginView(this, buf, XUtil::XML_FLAG_JSON_STRING);
	if (view_) {
		//Freeze();
		auto sizer = GetSizer();
		//view->Move(view_->GetPosition());
		//view->SetSize(view_->GetSize());
		sizer->Replace(view_, view, true);
		delete view_;
		Layout();
		//Refresh();
		//Thaw();
	}
	view_ = view;
}

void MyLoginDlg::OnCmbTypeUpdate(wxCommandEvent& event)
{
	UpdateType();
}

void MyLoginDlg::OnOK(wxCommandEvent& event)
{
	wxGetApp().Post(200, [this]() {
		user_ = view_->StartLogin([this](const zqdb::Msg& rsp) {
			stc_tips_->SetLabel(utf2wxString(rsp.GetErrorMsg()));
			if (rsp.GetErrorCode() != ZQDB_STATUS_OK) {
				user_ = nullptr;
				btn_ok_->Enable(true);
			}
			else if (rsp.IsContinue()) {

			}
			else {
				EndModal(wxID_OK);
			}
		});
		if (!user_) {
			stc_tips_->SetLabel(wxT("��¼ʧ��!!!"));
			return;
		}
		btn_ok_->Enable(false);
		//btn_cancel_->Enable(false);
		//EndModal(wxID_OK);
	});
}

void MyLoginDlg::OnIgnore(wxCommandEvent& event)
{
	EndModal(wxID_OK);
}

void MyLoginDlg::OnCancel(wxCommandEvent& WXUNUSED(event))
{
	if (user_) {
		view_->CancelLogin();
	}
	EndModal(wxID_CANCEL);
}



