#pragma once

#include "mybaseframe.h"
#include <wx/webview.h>

class MyWebFrame;

class MyWebFrame : public wxFrame
	, public zqdb::SkinMap<MyWebFrame, SkinInfo>
{
	typedef wxFrame Base;
	typedef zqdb::SkinMap<MyBaseFrame, SkinInfo> SkinBase;
protected:
	wxWebView* m_browser;
public:
	MyWebFrame(const char* xml, size_t xmlflag);
	~MyWebFrame();

	int FilterEvent(wxEvent& event);

	void LoadURL(const wxString& url);
	
	void OnSkinInfoChanged();
	
protected:
	//
	void OnNavigationRequest(wxWebViewEvent& evt);
	void OnNavigationComplete(wxWebViewEvent& evt);
	void OnDocumentLoaded(wxWebViewEvent& evt);
	void OnNewWindow(wxWebViewEvent& evt); 
	void OnTitleChanged(wxWebViewEvent& evt);
	void OnError(wxWebViewEvent& evt);
	wxDECLARE_EVENT_TABLE();
};

