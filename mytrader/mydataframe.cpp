#include "myapp.h"
#include "mydataframe.h"
#include <mdb.pb.h>

// ----------------------------------------------------------------------------
// MyDownloadDlg implementation
// ----------------------------------------------------------------------------

wxBEGIN_EVENT_TABLE(MyDownloadDlg, Base)
EVT_CLOSE(MyDownloadDlg::OnClose)
EVT_BUTTON(wxID_OK, MyDownloadDlg::OnOK)
EVT_BUTTON(wxID_CANCEL, MyDownloadDlg::OnCancel)
wxEND_EVENT_TABLE()

static MyDownloadDlg* g_download_dlg = nullptr;

void MyDownloadDlg::ShowDlg()
{
	if (!g_download_dlg)
		g_download_dlg = new MyDownloadDlg();
	g_download_dlg->Show();
}

void MyDownloadDlg::DestroyDlg()
{
	if (g_download_dlg) {
		delete g_download_dlg;
		wxASSERT(!g_download_dlg);
	}
}

MyDownloadDlg::MyDownloadDlg(wxWindow *parent)
	: Base(parent, wxID_ANY, wxT("盘后数据下载"), wxDefaultPosition, wxDefaultSize)
{
	SetIcon(wxICON(mytrader));

	const wxString modes[] = {
		wxT("增量下载（自动下载需要更新的数据）")
		, wxT("完全下载（重新下载所有数据）")
	};
	radio_mode_ = new wxRadioBox(this, wxID_ANY,
		wxT("下载方式"),
		wxDefaultPosition, wxDefaultSize,
		2, modes);

	chk_ktype_[0] = new wxCheckBox(this, wxID_ANY, wxT("1分"));
	chk_ktype_[0]->SetValue(true);
	chk_ktype_[1] = new wxCheckBox(this, wxID_ANY, wxT("5分"));
	chk_ktype_[1]->SetValue(true);
	chk_ktype_[2] = new wxCheckBox(this, wxID_ANY, wxT("日线"));
	chk_ktype_[2]->SetValue(true);

	chk_cat_[0] = new wxCheckBox(this, wxID_ANY, wxT("股票"));
	chk_cat_[0]->Bind(wxEVT_CHECKBOX, &MyDownloadDlg::OnChkCat, this);
	chk_cat_[0]->SetValue(true); 
	chk_cat_[1] = new wxCheckBox(this, wxID_ANY, wxT("债券"));
	chk_cat_[1]->Bind(wxEVT_CHECKBOX, &MyDownloadDlg::OnChkCat, this);
	chk_cat_[1]->SetValue(true);
	chk_cat_[2] = new wxCheckBox(this, wxID_ANY, wxT("基金"));
	chk_cat_[2]->Bind(wxEVT_CHECKBOX, &MyDownloadDlg::OnChkCat, this);
	chk_cat_[2]->SetValue(true);
	chk_cat_[3] = new wxCheckBox(this, wxID_ANY, wxT("期货"));
	chk_cat_[3]->Bind(wxEVT_CHECKBOX, &MyDownloadDlg::OnChkCat, this);
	chk_cat_[3]->SetValue(true);

	ctrl_cat_ = new MyScrollT<wxDataViewCtrl>(this, wxID_ANY, wxDefaultPosition, wxSize(600, 400)
		, wxNO_BORDER);
	//ctrl_cat_->ShowScrollbars(wxSHOW_SB_NEVER, wxSHOW_SB_DEFAULT);
	//ctrl_cat_->Bind(wxEVT_CHAR, &MyDataFrame::OnDataViewChar, this);

	ctrl_cat_model_ = new MyCatModel();
	ctrl_cat_->AssociateModel(ctrl_cat_model_.get());

#if wxUSE_DRAG_AND_DROP && wxUSE_UNICODE
	ctrl_cat_->EnableDragSource(wxDF_UNICODETEXT);
	ctrl_cat_->EnableDropTarget(wxDF_UNICODETEXT);
#endif // wxUSE_DRAG_AND_DROP && wxUSE_UNICODE

	// column 0 of the view control:

	wxDataViewTextRenderer *tr =
		new wxDataViewTextRenderer("string", wxDATAVIEW_CELL_INERT);
	wxDataViewColumn *column0 =
		new wxDataViewColumn("名称", tr, 0, FromDIP(200), wxALIGN_LEFT,
			wxDATAVIEW_COL_SORTABLE | wxDATAVIEW_COL_RESIZABLE);
	ctrl_cat_->AppendColumn(column0);

	// column 1 of the view control:

	ctrl_cat_->AppendToggleColumn(L"\u2714",
		1,
		wxDATAVIEW_CELL_ACTIVATABLE,
		wxCOL_WIDTH_AUTOSIZE);

	// column 2 of the view control:

	ctrl_cat_->AppendProgressColumn("进度", 2, wxDATAVIEW_CELL_INERT, FromDIP(80));

	// column 3 of the view control:

	tr = new wxDataViewTextRenderer("string", wxDATAVIEW_CELL_INERT);
	wxDataViewColumn *column3 =
		new wxDataViewColumn("信息", tr, 3, FromDIP(150), wxALIGN_LEFT,
			wxDATAVIEW_COL_SORTABLE | wxDATAVIEW_COL_REORDERABLE |
			wxDATAVIEW_COL_RESIZABLE);
	column3->SetMinWidth(FromDIP(150)); // this column can't be resized to be smaller
	ctrl_cat_->AppendColumn(column3);
#ifdef _DEBUG
	auto scrollbar = new MyScrollBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSB_VERTICAL);
	((MyScrollT<wxDataViewCtrl>*)ctrl_cat_)->SetScrollBar(nullptr, scrollbar);
	const auto header = ctrl_cat_->GenericGetHeader();
	if (header) {
		SetHeaderCustomDraw(header, [this, header](wxDC &dc, const wxRect& rect) {
			dc.GradientFillLinear(rect, *wxBLUE, *wxBLACK, wxEAST);
			auto rcOffset = rect;
			auto count = header->GetColumnCount();
			for (auto i = 0; i < count; i++)
			{
				auto col = ctrl_cat_->GetColumnAt(i);
				auto rcCalc = rcOffset;
				dc.DrawLabel(col->GetTitle(), wxNullBitmap, rcCalc
					, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL, -1, &rcCalc);
				rcOffset.x += col->GetWidth();
				rcOffset.width -= col->GetWidth();
				dc.DrawLine(rcOffset.GetLeftTop(), rcOffset.GetLeftBottom());
			}
		});
	}
#endif

	UpdateStock();
	UpdateBond();
	UpdateFund();
	UpdateFutures();

	btn_ok_ = new wxButton(this, wxID_OK, wxT("开始下载"));
	btn_cancel_ = new wxButton(this, wxID_CANCEL, _("&Cancel"));

	wxBoxSizer *sizerTop = new wxBoxSizer(wxVERTICAL);

	// mode
	auto sizer_mode_ = new wxBoxSizer(wxHORIZONTAL);
	sizer_mode_->Add(radio_mode_, wxSizerFlags(0).Expand());
	sizerTop->Add(sizer_mode_, wxSizerFlags(0).Expand().Border(wxUP, 5));

	// ktype
	auto *boxKType = new wxStaticBox(this, wxID_ANY, wxT("周期"));
	auto *sizerKType = new wxStaticBoxSizer(boxKType, wxHORIZONTAL);
	sizerKType->Add(chk_ktype_[0], wxSizerFlags(0).Expand().Border(wxALL, 5));
	sizerKType->Add(chk_ktype_[1], wxSizerFlags(0).Expand().Border(wxALL, 5));
	sizerKType->Add(chk_ktype_[2], wxSizerFlags(0).Expand().Border(wxALL, 5));
	sizerTop->Add(sizerKType, wxSizerFlags(0).Expand().Border(wxUP, 5));

	// cat
	auto *boxCat = new wxStaticBox(this, wxID_ANY, wxT("类别"));
	auto *sizerCat = new wxStaticBoxSizer(boxCat, wxHORIZONTAL);
	sizerCat->Add(chk_cat_[0], wxSizerFlags(0).Expand().Border(wxALL, 5));
	sizerCat->Add(chk_cat_[1], wxSizerFlags(0).Expand().Border(wxALL, 5));
	sizerCat->Add(chk_cat_[2], wxSizerFlags(0).Expand().Border(wxALL, 5));
	sizerCat->Add(chk_cat_[3], wxSizerFlags(0).Expand().Border(wxALL, 5));
	sizerTop->Add(sizerCat, wxSizerFlags(0).Expand().Border(wxUP, 5));

	// category
	auto *boxCategory = new wxStaticBox(this, wxID_ANY, wxT("品种"));
	auto *sizerCategory = new wxStaticBoxSizer(boxCategory, wxHORIZONTAL);
	sizerCategory->Add(ctrl_cat_, 1, wxEXPAND);
#ifdef _DEBUG
	sizerCategory->Add(scrollbar, 0, wxEXPAND);
#endif
	sizerTop->Add(sizerCategory, wxSizerFlags(1).Expand());

	auto *sizerBottom = new wxBoxSizer(wxHORIZONTAL);
	sizerBottom->Add(new wxStaticText(this, wxID_ANY, wxT("下载数据可能需要很长时间，请耐心等待，您可以继续使用软件其他功能。"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER_VERTICAL), 1, wxALIGN_CENTER | wxALL, 5);
	//sizerBottom->AddStretchSpacer(1);
	sizerBottom->Add(btn_ok_, 0, wxALIGN_CENTER | wxALL, 5);
	sizerBottom->Add(btn_cancel_, 0, wxALIGN_CENTER | wxALL, 5);
	//sizerBottom->AddStretchSpacer(1);
	sizerTop->Add(sizerBottom, 0, wxEXPAND);

	SetSizerAndFit(sizerTop);

	Centre();

	//SetEscapeId(wxID_CANCEL);

	btn_ok_->SetFocus();
	btn_ok_->SetDefault();
}

MyDownloadDlg::~MyDownloadDlg()
{
	g_download_dlg = nullptr; 
	Stop(2);
}

void MyDownloadDlg::UpdateStock()
{
	ctrl_cat_model_->RemoveRoot(wxT("股票"));
	if (chk_cat_[0]->GetValue()) {
		char type[1] = {
			PRODUCT_TYPE_Stock
		};
		auto item = ctrl_cat_model_->AddRoot(wxT("股票"), type, 1);
		ctrl_cat_->Expand(item);
	}
}

void MyDownloadDlg::UpdateBond()
{
	ctrl_cat_model_->RemoveRoot(wxT("债券"));
	if (chk_cat_[1]->GetValue()) {
		char type[1] = {
			PRODUCT_TYPE_Bond
		};
		auto item = ctrl_cat_model_->AddRoot(wxT("债券"), type, 1);
		ctrl_cat_->Expand(item);
	}
}

void MyDownloadDlg::UpdateFund()
{
	ctrl_cat_model_->RemoveRoot(wxT("基金"));
	if (chk_cat_[2]->GetValue()) {
		char type[1] = {
			PRODUCT_TYPE_Fund
		};
		auto item = ctrl_cat_model_->AddRoot(wxT("基金"), type, 1);
		ctrl_cat_->Expand(item);
	}
}

void MyDownloadDlg::UpdateFutures()
{
	ctrl_cat_model_->RemoveRoot(wxT("期货"));
	if (chk_cat_[3]->GetValue()) {
		char type[1] = {
			PRODUCT_TYPE_Futures
		};
		auto item = ctrl_cat_model_->AddRoot(wxT("期货"), type, 1);
		ctrl_cat_->Expand(item);
	}
}

void MyDownloadDlg::OnChkCat(wxCommandEvent& event)
{
	if (event.GetId() == chk_cat_[0]->GetId()) {
		UpdateStock();
	} else if (event.GetId() == chk_cat_[1]->GetId()) {
		UpdateBond();
	}
	else if (event.GetId() == chk_cat_[2]->GetId()) {
		UpdateFund();
	}
	else if (event.GetId() == chk_cat_[3]->GetId()) {
		UpdateFutures();
	}
}

void MyDownloadDlg::Stop(int flag)
{
	if (!thread_) {
		return;
	}
	stop_flag_ = flag;
	thread_->join();
	delete thread_;
	thread_ = nullptr;
}

void MyDownloadDlg::OnClose(wxCloseEvent& event)
{
	if (event.CanVeto() && thread_) {
		if (wxOK != wxMessageBox(wxT("正在下载数据中...\n确定要关闭窗口停止下载吗？"),
			wxT("提示"),wxOK | wxCANCEL | wxICON_INFORMATION, this)) {
			event.Veto();
			return;
		}
	}
	event.Veto();
	delete this;
}

void MyDownloadDlg::OnOK(wxCommandEvent& event)
{
	if (thread_) {
		btn_ok_->SetLabel(wxT("正在停止"));
		btn_ok_->Update();
		Stop();
	}
	else {
		auto dir = ZQDBGetMarketDir();
		if (!dir || !dir[0]) {
			wxMessageBox(wxT("没有指定行情数据路径，请重新设置!!!"),
				wxT("提示"), wxOK | wxICON_INFORMATION, this);
			return;
		}
		auto mode = radio_mode_->GetSelection();
		std::array<PERIODTYPE, 3> ktypes = { CYC_1MIN, CYC_5MIN, CYC_DAY };
		std::vector<PERIODTYPE> periods;
		for (size_t i = 0; i < chk_ktype_.size(); i++) {
			if (chk_ktype_[i]->GetValue()) {
				periods.push_back(ktypes[i]);
			}
		}
		std::vector<wxDataViewItem> items;
		auto roots = ctrl_cat_model_->GetRoots();
		for (auto root : roots) {
			wxDataViewItemArray childs;
			ctrl_cat_model_->GetChildren(root, childs);
			for (auto child : childs)
			{
				ctrl_cat_model_->UpdateProgress(child, 0);
				ctrl_cat_model_->UpdateInfo(child, wxEmptyString);
				if (ctrl_cat_model_->GetState(child)) {
					items.push_back(child);
				}
			}
		}
		if (periods.empty()) {
			wxMessageBox(wxT("请选择下载数据周期!!!"),
				wxT("提示"), wxOK | wxICON_INFORMATION, this);
			return;
		}
		if (items.empty()) {
			wxMessageBox(wxT("请选择下载数据品种!!!"),
				wxT("提示"), wxOK | wxICON_INFORMATION, this);
			return;
		}
		else {
			//时间段判断
			bool close_flag = false;
			for (auto item : items)
			{
				HZQDB hProduct = ctrl_cat_model_->GetHandle(item);
				wxASSERT(hProduct);
				uint32_t tradeday = 0, date = 0, time = 0;
				time = ZQDBGetNowTime(hProduct, &date, &tradeday);
				uint32_t close_date = 0, close_time = 0;
				close_time = ZQDBGetCloseTime(hProduct, &close_date, (size_t)-1);
				if ((date > close_date) || (date == close_date && time >= close_time)) {
					//
				}
				else {
					close_flag = true;
				}
				if (close_flag) {
					wxMessageBox(wxT("盘中不可以下载数据，请收盘后下载！！！"),
						wxT("提示"), wxOK | wxICON_INFORMATION, this);
					return;
				}
			}
		}
		//
		radio_mode_->Disable();
		for (auto one : chk_ktype_) {
			one->Disable();
		}
		for (auto one : chk_cat_) {
			one->Disable();
		}
		ctrl_cat_model_->EnableState(false);
		ctrl_cat_->Refresh();
		btn_ok_->SetLabel(wxT("停止下载"));
		//
		wxASSERT(!thread_);
		stop_flag_ = false;
		thread_ = new std::thread([this, dir, mode, periods, items] {
			for (auto item : items)
			{
				if (stop_flag_) {
					break;
				}
				HZQDB hProduct = ctrl_cat_model_->GetHandle(item);
				wxASSERT(hProduct);
				zqdb::AllCode allcode(hProduct);
				for (size_t i = 0, j = allcode.size(); i < j; i++)
				{
					if (stop_flag_) {
						break;
					}
					wxGetApp().Post([this, item, i, j] {
						if (!g_download_dlg) {
							return;
						}
						ctrl_cat_model_->UpdateProgress(item, ceil(((i + 1) * 100) / j));
					});
					zqdb::Code code(allcode[i]);
					for (auto cycle : periods)
					{
						wxString info;
						bool need_request = true;
						if (mode == 0) {
							//判断本地是否是最新数据
							if (ZQDBExistData(code, cycle)) {
								info = wxString::Format(wxT("%zu/%zu %s.%s周期%s数据无需下载"), i + 1, j, code->Code, code->Exchange, _(ZQDBCycle2Str(cycle)));
								need_request = false;
							}
						}
						if (need_request) {
							try {
								HNMSG rsp = nullptr;
								ZQDBRequestData(code, cycle, &rsp, 5000);
								if (stop_flag_) {
									break;
								}
								if (rsp) {
									zqdb::Msg rsp_msg(rsp, zqdb::Msg::AutoDelete);
									if (!rsp_msg.IsError()) {
										info = wxString::Format(wxT("%zu/%zu %s.%s周期%s数据下载成功"), i + 1, j, code->Code, code->Exchange, _(ZQDBCycle2Str(cycle)));
										ZQDBSaveData(code, cycle, rsp);
									}
									else {
										info = wxString::Format(wxT("%zu/%zu %s.%s周期%s数据下载出错:%d %s"), i + 1, j, code->Code, code->Exchange, _(ZQDBCycle2Str(cycle)), rsp_msg.GetErrorCode(), utf2wxString(rsp_msg.GetErrorMsg()));
									}
								}
								else {
									info = wxString::Format(wxT("%zu/%zu %s.%s周期%s数据下载超时！！！"), i + 1, j, code->Code, code->Exchange, _(ZQDBCycle2Str(cycle)));
								}
							}
							catch (...) {
								info = wxString::Format(wxT("%zu/%zu %s.%s周期%s数据下载异常！！！"), i + 1, j, code->Code, code->Exchange, _(ZQDBCycle2Str(cycle)));
							}
						}
						wxGetApp().Post([this, item, info] {
							if (!g_download_dlg) {
								return;
							}
							ctrl_cat_model_->UpdateInfo(item, info);
						});
					}
				}
			}
			if (stop_flag_ != 2) {
				wxGetApp().Post([this] {
					if (!g_download_dlg) {
						return;
					}
					radio_mode_->Enable();
					for (auto one : chk_ktype_) {
						one->Enable();
					}
					for (auto one : chk_cat_) {
						one->Enable();
					}
					ctrl_cat_model_->EnableState();
					ctrl_cat_->Refresh();
					btn_ok_->SetLabel(wxT("开始下载"));
					if (!stop_flag_) {
						wxMessageBox(wxT("下载完成"),
							wxT("提示"), wxOK | wxICON_INFORMATION, this);
					}
				});
			}
		});
	}
}

void MyDownloadDlg::OnCancel(wxCommandEvent& event)
{
	delete this;
}

///
wxBEGIN_EVENT_TABLE(MyDataFrame, wxFrame)
//EVT_CLOSE(MyDataFrame::OnCloseEvent)
//EVT_IDLE(MyDataFrame::OnIdle)
wxEND_EVENT_TABLE()

MyDataFrame::MyDataFrame(const char* xml, size_t xmlflag)
	: wxFrame(NULL, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize)
{
	SetIcon(wxICON(mytrader));
}

MyDataFrame::~MyDataFrame()
{
	wxGetApp().ResetDataFrame();
}

int MyDataFrame::FilterEvent(wxEvent& event)
{
	// Continue processing the event normally as well.
	return wxEventFilter::Event_Skip;
}

void MyDataFrame::OnSkinInfoChanged()
{
	Freeze();
	//
	Layout();
	Thaw();
}
