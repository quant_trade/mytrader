#pragma once

#include "mybaseframe.h"

class MyDataFrame;

class MyDownloadDlg : public wxDialog
{
	typedef wxDialog Base;
private:
	wxRadioBox *radio_mode_ = nullptr;
	std::array<wxCheckBox*, 3> chk_ktype_;
	std::array<wxCheckBox*, 4> chk_cat_;
	wxDataViewCtrl* ctrl_cat_ = nullptr;
	wxObjectDataPtr<MyCatModel> ctrl_cat_model_;
	std::thread* thread_ = nullptr;
	int stop_flag_ = 0; //0:��ֹͣ��1��ֹͣ��2��ֹͣ���رնԻ���
	wxButton* btn_ok_ = nullptr;
	wxButton* btn_cancel_ = nullptr;
public:
	static void ShowDlg();
	static void DestroyDlg();

	MyDownloadDlg(wxWindow *parent = nullptr);
	virtual ~MyDownloadDlg();

protected:
	//
	void UpdateStock();
	void UpdateBond();
	void UpdateFund();
	void UpdateFutures();
	void OnChkCat(wxCommandEvent& event);
	void Stop(int flag = 1);
	void OnClose(wxCloseEvent& event);
	void OnOK(wxCommandEvent& event);
	void OnCancel(wxCommandEvent& event);

	wxDECLARE_EVENT_TABLE();
};

class MyDataFrame : public wxFrame
	, public zqdb::SkinMap<MyDataFrame, SkinInfo>
{
	typedef wxFrame Base;
	typedef zqdb::SkinMap<MyBaseFrame, SkinInfo> SkinBase;
protected:
public:
	MyDataFrame(const char* xml, size_t xmlflag);
	~MyDataFrame();

	int FilterEvent(wxEvent& event);

	void OnSkinInfoChanged();
	
protected:
	//
	wxDECLARE_EVENT_TABLE();
};

