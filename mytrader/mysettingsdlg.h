#pragma once

#include "utility.h"

#include "wx/richtext/richtextctrl.h"
//#include "wx/richtext/richtextstyles.h"
//#include "wx/richtext/richtextxml.h"
//#include "wx/richtext/richtexthtml.h"
//#include "wx/richtext/richtextformatdlg.h"
//#include "wx/richtext/richtextsymboldlg.h"
//#include "wx/richtext/richtextstyledlg.h"
//#include "wx/richtext/richtextprint.h"
//#include "wx/richtext/richtextimagedlg.h"

#include "wx/datectrl.h"
#include "wx/timectrl.h"
#include "wx/dateevt.h"

enum
{
	SETTINGS_NORMAL = 0,
	SETTINGS_GUEST,
	SETTINGS_USER,
	SETTINGS_TEST,
	SETTINGS_FULL,
};

class MySettingsDlg : public wxDialog
{
	typedef wxDialog Base;
private:
	wxRichTextCtrl* ctrl_text_ = nullptr;
	//wxTextCtrl* ctrl_text_ = nullptr;
	wxButton* btn_ok_ = nullptr;
	wxButton* btn_cancel_ = nullptr;
public:
	static std::string GetFile(int type = SETTINGS_NORMAL);
	static boost::property_tree::ptree GetConfig(int type = SETTINGS_NORMAL);
	static bool HealthCheck(const std::string& settings_file);
	static bool HealthCheck(const boost::property_tree::ptree& cfg);
	MySettingsDlg(boost::property_tree::ptree& cfg);
	virtual ~MySettingsDlg();
protected:
	boost::property_tree::ptree& cfg_;
	void OnOK(wxCommandEvent& event);
	void OnCancel(wxCommandEvent& event);

	wxDECLARE_EVENT_TABLE();
};

class PrefsPageGeneralPanel : public wxPanel
{
public:
	PrefsPageGeneralPanel(wxWindow *parent);

	virtual bool TransferDataToWindow() wxOVERRIDE;
	virtual bool TransferDataFromWindow() wxOVERRIDE;

private:
	void UpdateSettingsIfNecessary();
	void ChangedUseMarkdown(wxCommandEvent& e);
	void ChangedSpellcheck(wxCommandEvent& e);

	wxCheckBox *m_useMarkdown;
	wxCheckBox *m_spellcheck;

	// Settings corresponding to the current values in this dialog.
	//MySettings m_settingsCurrent;
};

class PrefsPageGeneral : public wxStockPreferencesPage
{
public:
	PrefsPageGeneral() : wxStockPreferencesPage(Kind_General) {}
	virtual wxWindow *CreateWindow(wxWindow *parent) wxOVERRIDE
	{
		return new PrefsPageGeneralPanel(parent);
	}
};


class PrefsPageAboutPanel : public wxPanel
{
public:
	PrefsPageAboutPanel(wxWindow *parent);
};

class PrefsPageAbout : public wxPreferencesPage
{
public:
	virtual wxString GetName() const wxOVERRIDE { return _("About"); }
	virtual wxBitmap GetLargeIcon() const wxOVERRIDE
	{
		return wxArtProvider::GetBitmap(wxART_HELP, wxART_TOOLBAR);
	}
	virtual wxWindow *CreateWindow(wxWindow *parent) wxOVERRIDE
	{
		return new PrefsPageAboutPanel(parent);
	}
};

class MyTechDlg : public wxDialog
{
	typedef wxDialog Base;
protected:
#if USE_CYC_SEC
	wxSpinCtrl *ctrl_cycle_anysec_;
#endif
	wxSpinCtrl *ctrl_cycle_anymin_;
public:
	MyTechDlg(wxWindow *parent);

protected:
	void OnOK(wxCommandEvent& event);
	void OnCancel(wxCommandEvent& event);

	wxDECLARE_EVENT_TABLE();
};

class MyAboutDlg : public wxDialog
{
	typedef wxDialog Base;
public:
	MyAboutDlg(wxWindow *parent);
protected:
	//
	void OnOK(wxCommandEvent& event);
	void OnCancel(wxCommandEvent& event);

	wxDECLARE_EVENT_TABLE();
};

class MyCheckDlg : public wxDialog
{
	typedef wxDialog Base;
private:
	wxStaticText* ctrl_text_ = nullptr;
	wxCheckBox* ctrl_check_ = nullptr;
	wxButton* btn_ok_ = nullptr;
	wxButton* btn_cancel_ = nullptr;
public:
	MyCheckDlg(wxWindow* parent, const wxString& text, const wxString& check_text, bool check_value, const wxString& title, long style = wxOK|wxCANCEL);

	bool IsCheck();
protected:
	void OnOK(wxCommandEvent& event);
	void OnCancel(wxCommandEvent& event);

	wxDECLARE_EVENT_TABLE();
};

class MyTestDlg : public wxDialog
{
	typedef wxDialog Base;
protected:
	enum {
		CTRL_ID_DATE = wxID_HIGHEST + 1,
		CTRL_ID_TIME,
		CTRL_ID_SPEED,
	};
	wxDatePickerCtrl *ctrl_date_ = nullptr;
	wxTimePickerCtrl *ctrl_time_ = nullptr;
	wxSlider *ctrl_speed_ = nullptr;
	wxButton* btn_ok_ = nullptr;
	wxButton* btn_cancel_ = nullptr;
public:
	MyTestDlg(wxWindow *parent, const wxString& title, uint32_t date, uint32_t time);

	uint32_t GetDate();
	uint32_t GetTime();
	size_t GetSpeed();

protected:

	void OnSpeed(wxScrollEvent& event);

	void OnOK(wxCommandEvent& event);
	void OnCancel(wxCommandEvent& event);

	wxDECLARE_EVENT_TABLE();
};

class MyTradeTestDlg : public wxDialog
{
	typedef wxDialog Base;
protected:
	zqdb::Calc::AllFunc all_strategy_;
	enum {
		CTRL_ID_DATE = wxID_HIGHEST + 1,
		CTRL_ID_TIME,
		CTRL_ID_SPEED,
		CTRL_ID_AMOUNT,
		CTRL_ID_STRATEGY,
	};
	wxDatePickerCtrl *ctrl_date_ = nullptr;
	wxTimePickerCtrl *ctrl_time_ = nullptr;
	wxSlider *ctrl_speed_ = nullptr;
	wxSpinCtrlDouble *ctrl_amount_ = nullptr;
	wxCheckListBox* ctrl_strategy_ = nullptr;
	wxButton* btn_ok_ = nullptr;
	wxButton* btn_cancel_ = nullptr; 
public:
	MyTradeTestDlg(wxWindow *parent, const wxString& title, uint32_t date, uint32_t time);

	uint32_t GetDate();
	uint32_t GetTime();
	size_t GetSpeed();
	double GetAmount();
	std::vector<wxString> GetStrategy();

protected:

	void OnCheckStrategy(wxCommandEvent& event);

	void OnOK(wxCommandEvent& event);
	void OnCancel(wxCommandEvent& event);

	wxDECLARE_EVENT_TABLE();
};

class MyDepthTestDlg : public wxDialog
{
	typedef wxDialog Base;
protected:
	enum {
		CTRL_ID_SPEED = wxID_HIGHEST + 1,
	};
	wxSlider *ctrl_speed_ = nullptr;
	wxButton* btn_ok_ = nullptr;
	wxButton* btn_cancel_ = nullptr;
public:
	MyDepthTestDlg(wxWindow *parent);

	size_t GetSpeed();

protected:

	void OnOK(wxCommandEvent& event);
	void OnCancel(wxCommandEvent& event);

	wxDECLARE_EVENT_TABLE();
};
